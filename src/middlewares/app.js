import { apiRequest } from "../actions/api";
import { LOGIN, GET_ME, setUser, LOGOUT } from "../actions/auth";
import request from "../utils/request";
import { cache } from 'swr';
// import * as Sentry from '@sentry/react';

export const appMiddleware = ({ dispatch }) => next => action => {
    next(action);
    switch (action.type) {
        case LOGOUT: {
            console.log('LOGOUT!')
            cache.clear();
            break;
        }
        case LOGIN: {
            next(
                apiRequest({
                    url: `/login`,
                    data: action.payload
                })
            );
            break;
        }
        case GET_ME: {
            request.get(`/me`)
                .then(response => {
                    dispatch(setUser(response.data.data))
                })
            break;
        }
        default:
            break;
    }
};
