import React from 'react'
import {Card, CardBody, Button} from 'reactstrap'

function OpeningRMIB({submit}){

    return(
        <Card>
                <CardBody className="p-5 text-center">
                    <h3>
                        Tes RMIB
                    </h3>
                    <div className='my-5'>
                        <h5>Petunjuk Pengerjaan Tes RMIB</h5>
                        <div className="mt-2 w-75 mx-auto d-flex justify-content-center align-items-center">
                            <img src={require('../../../assets/work.svg')} alt="dashboard" width={200} />
                        </div>
                        <ol className='text-left my-3'>
                            <li>Pada Tes kedua, terdapat 8 macam sub tes yang akan dikerjakan</li>
                            <li>Pada masing-masing usbtes, tidak ada batas waktu pengerjaan</li>
                            <li>Pada masing-masing subtes, terdapat 12 macam pekerjaan. Setiap pekerjaan merupakan keahlian 
                                khusus yang memerlukan latihan atau pendidikan tersendiri, dan mungkin hanya beberapa 
                                pekerjaan atau bidang yang anda minati
                            </li>
                            <li>
                                Disini Anda diminta untuk memilih pekerjaan mana yang ingin Anda lakukan atau 
                                pekerjaan mana yang Anda sukai, terlepas dari besarnya upah atau gaji yang akan diterima. 
                                Juga terlepas dari apakah Anda akan berhasil atau tidak dalam mengerjakan pekerjaan tersebut.
                            </li>
                            <li>
                                Tugas Anda adalah mencantumkan nomor atau angka pada tiap pekerjaan dalam 
                                kelompok-kelompok yang tersedia. Berikanlah nomor (angka) 1 untuk 
                                pekerjaan yang paling Anda sukai diantara keduabelas pekerjaan 
                                yang tersedia pada setiap kelompok dan dilanjutkan dengan 
                                pemberian nomor-nomor 2; 3; dan seterusnya berurutan berdasarkaan 
                                besarnya kadar kesukaan/minat Anda terhadap pekerjaan itu dan nomor 
                                (angka) 12 anda cantumkan untuk pekerjaan yang paling tidak disukai 
                                dari daftar pekerjaan yang tersedia pada kelompok tersebut.
                            </li>
                            <li>
                                Bekerjalah secepatnya dan tulislah nomor-nomor (angka-angka) sesuai dengan 
                                kesan dan keinginan Anda yang pertama muncul.
                            </li>
                            <li>Anda dapat menuju ke subtes sebelumnya, dan subtes selanjutnya jika anda sudah mengisi seluruh pekerjaan</li>
                        </ol>
                    </div>
                    <div className='mt-5 text-center'>
                        <Button onClick={submit} color="main-rounded">Mulai Tes</Button>
                    </div>
                </CardBody>
            </Card>
    )
}

export default OpeningRMIB