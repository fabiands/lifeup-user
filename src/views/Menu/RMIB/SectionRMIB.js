import React, {useState, useEffect} from 'react'
import {Card, CardBody, Button, Table, Input, Label, Row, Col, Modal, ModalBody, ModalHeader, ModalFooter} from 'reactstrap'
import LoadingSpin from '../../../components/LoadingSpin'
import ModalError from '../../../components/ModalError';
import request from "../../../utils/request"
import { toast } from 'react-toastify'


function SectionRMIB({section, submit, prev}){
    const sec = 'jobs'
    const [data, setData] = useState([])
    const half = Math.ceil(data?.length / 2);    
    const firstHalf = data?.slice(0, half)
    const secondHalf = data?.slice(-half)
    const divide = [firstHalf, secondHalf]
    const local = localStorage.getItem('rmib')
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [answer, setAnswer] = useState(local ? JSON.parse(local)[section] : [])
    const [duplicate, setDuplicate] = useState(false)
    const [outRange, setOutRange] = useState(false)
    const nullValue = answer? (Object.values(answer).length < data?.length) : false
    const [dataSubmit, setDataSubmit] = useState(null)
    const [modalPost, setModalPost] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [dataJobs, setDataJobs] = useState(local ? (JSON.parse(local)[sec] ?? {0:'',1:'',2:''}) : {0:'',1:'',2:''})
    const [noJobs, setNoJobs] = useState(false)
    const fillForm = section === 'i' ? Object.values(dataJobs).every(item => Boolean(item)) : true

    const handleChange = (id, value) => {
        let val = value || 0
        const newAnswer = { ...answer, [id]: parseFloat(val) }
        setAnswer(newAnswer)
    }

    const handleChangeJobs = (id, value) => {
        const newJob = { ...dataJobs, [id]:value}
        setDataJobs(newJob)
    }

    const handleNumberOnly = (evt) => {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            evt.preventDefault()
        }
        return true;
    }

    useEffect(() => {
        if(noJobs){
            const interval = setInterval(() => {
                setNoJobs(false)
            }, 2000);
            return () => clearInterval(interval);
        }
    }, [noJobs]);

    useEffect(() => {
        if(section !== 'opening' && section !== 'end'){
            setLoading(true)
            request.get(`rmib?section=${section}`)
                .then((res) => setData(res.data.data))
                .catch(() => setError(true))
                .finally(() => setLoading(false))
        }
    }, [section])

    // useEffect(() => {
    //     let arr = answer ? Object.values(answer) : []
    //     let set = new Set(arr)
    //     if(set.size !== arr.length){
    //         setDuplicate(true)
    //     }
    //     else{
    //         setDuplicate(false)
    //     }
    // }, [answer])

    // useEffect(() => {
    //     let arr = answer ? Object.values(answer) : []
    //     let err = arr?.filter(i => (i<=0 || i>12))
    //     if(err.length > 0){
    //         setOutRange(true)
    //     }
    //     else{
    //         setOutRange(false)
    //     }
    // }, [answer])

    const handleCheck = () => {
        let arr = answer ? Object.values(answer) : []
        let set = new Set(arr)
        if(set.size !== arr.length){
            setDuplicate(true)
        }
        else{
            setDuplicate(false)
        }

        let err = arr?.filter(i => (i<=0 || i>12))
        if(err.length > 0){
            setOutRange(true)
        }
        else{
            setOutRange(false)
        }
    }

    useEffect(() => {
        setAnswer(local ? (JSON.parse(local)[section] ?? []) : [])
    }, [local, section])

    const handleSubmit = () => {
        const arr = Object.values(answer)
        const parse = JSON.parse(local)
        let newAnswers = {...parse, [section]: arr}
        
        if(section === 'i'){
            let job = Object.values(dataJobs)
            const sameJob = new Set(job).size !== job.length
            if(sameJob){
                setNoJobs(true)
                return;
            }
            // const nullJob = job.every(item => Boolean(item))
            // if(!nullJob){
            //     setNoJobs(true)
            //     return;
            // }
            newAnswers = {...newAnswers, [sec]:job}
            localStorage.setItem('rmib', JSON.stringify(newAnswers))
            setDataSubmit(JSON.stringify(newAnswers))
            setModalPost(true)
        }
        else{
            localStorage.setItem('rmib', JSON.stringify(newAnswers))
            submit()
        }
    }

    const handlePost = () => {
        setIsLoading(true)
        request.post('/rmib', JSON.parse(dataSubmit))
        .then(() => {
            toast.success('Success')
            submit()
        })
        .catch(() => {
            toast.error('Error')
            return;
        })
        .finally(() => setIsLoading(false))
    }

    return(
        <>
        <Card className="bg-white">
            {loading ? <LoadingSpin />
            : error ? <ModalError />
            :
            <CardBody className="p-3">
                <h3 className="mt-5 text-capitalize">Section {section}</h3>
                <h6 className='mb-2'><i>Masukkan angka 1 s.d. 12 berdasarkan prioritas</i></h6>
                <div className='d-flex flex-column text-center my-2'>
                    {duplicate && <small className='my-1 text-danger'>Tidak boleh ada nomor yang sama !</small>}
                    {outRange && <small className='my-1 text-danger'>Angka yang diperbolehkan hanya 1 s.d. 12 !</small>}
                </div>
                <Row className="my-2">
                {divide.map((datadiv, idx) =>
                    <Col key={idx} md='6' className="px-3">
                        <Table borderless>
                            <tbody>
                                {datadiv.map((item, index) =>
                                    <tr key={index+6*idx}>
                                        <td className='text-right pl-2 pt-3 text-capitalize'>
                                            <Label className="text-dark" htmlFor={item}>
                                                {item}
                                            </Label>
                                        </td>
                                        <td>
                                            <Input
                                                className="w-75"
                                                type="text"
                                                name={item}
                                                id={item}
                                                value={answer[index+6*idx] ?? ''}
                                                onKeyPress={handleNumberOnly}
                                                pattern="[0-9]*"
                                                inputMode="numeric"
                                                maxLength="2"
                                                onBlur={handleCheck}
                                                onChange={(e) => handleChange(index+6*idx, e.target.value)}
                                            />
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                    </Col>
                )}
                {section === 'i' &&
                    <Card className="mx-auto shadow-sm">
                        <CardBody className="p-5">
                            <h6 className='text-left'>
                                Tulislah dibawah ini 3 macam pekerjaan yang paling ingin anda lakukan atau paling anda sukai<br />
                                <i>(Tidak harus pekerjaan yang tercantum daftar yang ada)</i>
                            </h6>
                            <Table borderless className="w-50">
                                <tbody>
                                {Object.values(dataJobs).map((item, idx) =>
                                    <tr key={idx}>
                                        <td className='w-10 pt-3'>{idx+1}.</td>
                                        <td>
                                            <Input
                                                type="text"
                                                name={`job_${idx}`}
                                                id={`job_${idx}`}
                                                value={item}
                                                onChange={(e) => handleChangeJobs(idx, e.target.value)}
                                            />
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                            </Table>
                            {noJobs && 
                                <div className='w-100 d-flex justify-content-start'>
                                    <small className='my-1 text-danger'>Daftar Pekerjaan tidak boleh sama !</small>
                                </div>
                            }
                        </CardBody>
                    </Card>
                }
                </Row>
                <div className="d-flex justify-content-center">
                    {section !== 'a' &&
                        <Button className="mr-2" color="outline-rounded" onClick={prev}>
                            <i className='fa fa-arrow-left mr-2' /> Sebelumnya
                        </Button>
                    }
                    <Button className={section !== 'a' ? 'ml-2' : ''} disabled={nullValue || duplicate || outRange || !fillForm} color="main-rounded" onClick={handleSubmit}>
                        {section === 'i' ? 'Submit' : <>Selanjutnya <i className='fa fa-arrow-right ml-2' /></>}
                    </Button>
                </div>
            </CardBody>
            }
        </Card>
        <Modal isOpen={modalPost} className="rounded" style={{marginTop:'40vh'}}>
            <ModalHeader className="border-bottom-0 bg-transparent">
                Konfirmasi Submit Tes RMIB
            </ModalHeader>
            <ModalBody className="py-3 text-center">
                <h5>Apakah anda yakin ingin mengirim jawaban anda ?</h5>
                <h6 className='text-muted'>Anda dapat mengecek kembali jawaban anda</h6>
            </ModalBody>
            <ModalFooter className="border-top-0 text-center">
                <Button disabled={isLoading} color="netis-danger" className="mr-2" onClick={() => setModalPost(false)}>Batal</Button>
                <Button disabled={isLoading} className="ml-2" color="netis-color" onClick={handlePost}>
                    {isLoading ? 'loading...' : 'Submit'}
                </Button>
            </ModalFooter>
        </Modal>
        </>
    )

}

export default SectionRMIB