import React, { useState, useEffect } from 'react'
import { Prompt } from "react-router-dom";
import OpeningRMIB from "./OpeningRMIB"
import RMIBDone from './RMIBDone';
import SectionRMIB from './SectionRMIB';
import {useAuthUser} from '../../../store'

const sec = {
    0:'opening',
    1:'a',
    2:'b',
    3:'c',
    4:'d',
    5:'e',
    6:'f',
    7:'g',
    8:'h',
    9:'i',
    10:'end'
}

function RMIBWrapper(){
    const user = useAuthUser();
    const [activeSection, setActiveSection] = useState(0)
    const [clear, setClear] = useState(false)

    const submit = () => {
        setActiveSection(activeSection+1)
    }
    const prev = () => {
        setActiveSection(activeSection-1)
    }

    useEffect(() => {
        if(user.rmib){
            setActiveSection(10)
            setClear(true)
        }
        else{
            setActiveSection(0)
        }
    }, [user])

    return(
        <div className="page-content">
            <Prompt
                when={(window.onbeforeunload = () => true)}
                message={(location) => {
                    "Apakah kamu yakin ingin keluar ?"
                }}
            />
            {sec[activeSection] === 'opening' ? <OpeningRMIB submit={submit} />
            : sec[activeSection] === 'end' ? <RMIBDone clear={clear} />
            : <SectionRMIB section={sec[activeSection]} submit={submit} prev={prev} />
            }
            {/* {sec[activeSection] === 'opening' && <OpeningRMIB submit={submit} />}
            {sec[activeSection] === 'end' && <RMIBDone clear={clear} />}
            {(sec[activeSection] !== 'opening' && sec[activeSection] !== 'end') &&
                <SectionRMIB section={sec[activeSection]} submit={submit} prev={prev} />
            } */}
            
        </div>
    )
}

export default RMIBWrapper