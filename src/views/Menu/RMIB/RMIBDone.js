import React from 'react'
import { useHistory } from 'react-router-dom'
import { Button, Card, CardBody } from 'reactstrap'

function RMIBDone({clear}){
    const history = useHistory()

    const onDoneRMIB = () => {
        localStorage.removeItem('rmib')
        history.push('/')
    }

    return(
        <Card className="bg-white">
            <CardBody className="p-4">
                    <h3 className="my-5">
                    {clear ? 'Anda telah mengerjakan Test RMIB'
                    :
                    'Terima kasih telah mengerjakan Tes RMIB'
                    }
                    </h3>
                    <div className="mt-2 w-75 mx-auto d-flex justify-content-center align-items-center">
                        <img src={require('../../../assets/done.svg')} alt="dashboard" width={200} />
                    </div>
                    <div className='text-center w-75 mx-auto my-4'>
                        Terima kasih sudah mengikuti Assessment Test, harap menunggu admin memproses jawaban anda. 
                        Jika admin sudah selesai menganalisa jawaban anda, admin akan memberikan hasil berupa laporan atau 
                        feedback mengenai minat dan bakat anda.
                    </div>
                    <Button color="main-rounded" className="mb-4" onClick={onDoneRMIB}>Selesai</Button>
            </CardBody>
        </Card>
    )
}

export default RMIBDone