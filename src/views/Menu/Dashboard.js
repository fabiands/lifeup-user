import React, {useState, useEffect} from 'react'
import { Button, Card, CardBody, CardHeader, Collapse } from 'reactstrap'
import { Link, } from "react-router-dom";
import { useAuthUser } from '../../store';
import SupportDevice from '../../components/SupportDevice';
import LoadingSpin from '../../components/LoadingSpin';
import request from '../../utils/request';

function Dashboard() {
    const [loading, setLoading] = useState(false)
    const [dataUser, setDataUser] = useState([])
    const user = useAuthUser()
    const [ist, setIst] = useState(false)
    const toggle = () => {
        setIst(!ist)
    }

    useEffect(() => {
        setLoading(true)
        request.get('/me')
        .then((res) => {
            setDataUser(res.data.data)
        })
        .catch(() => {
            setDataUser(user)
        })
        .finally(() => setLoading(false))
        // eslint-disable-next-line
    },[])

    if(loading){
        return <LoadingSpin />
    }

    return (
        <div className="page-content">
            <SupportDevice isOpen={true} />
            <Card>
                <CardBody className="p-5 text-center">
                    <h3 className='mb-2'>
                        Selamat Datang di Web LifeUp Assessment Test
                    </h3>
                    <div className="mt-2 w-75 mx-auto d-flex justify-content-center align-items-center">
                        <img src={require('../../assets/dashboard.svg')} alt="dashboard" width={200} />
                    </div>
                    <div className="p-5 mx-auto" style={{width:'90%'}}>
                        <h6 className='text-left mt-2'>
                            LifeUp Assessment Test merupakan Web untuk pengerjaan Tes Minat dan Bakat anda, untuk membimbing 
                            anda menemukan potensi diri dan karir yang sesuai dengan kompetensi anda saat ini.
                        </h6>
                        {(dataUser?.ist && dataUser?.rmib) ?
                        <Card className="mt-3">
                            <CardBody>
                                <h4 className="my-4 w-75 text-center mx-auto">
                                    Anda telah mengerjakan seluruh Test
                                </h4>
                                <div className='text-center w-75 mx-auto my-4'>
                                    Terima kasih sudah mengikuti Assessment Test, harap menunggu admin memproses jawaban anda. 
                                    Jika admin sudah selesai menganalisa jawaban anda, admin akan memberikan hasil berupa laporan atau 
                                    feedback mengenai minat dan bakat anda.
                                </div>
                            </CardBody>
                        </Card>
                        :
                        <>
                        <Collapse isOpen={!ist}>
                            <h6 className='my-4 text-left'>
                                Sebelum mulai mengerjakan soal-soal Psikotes LifeUp, pastikan beberapa hal berikut.<br />
                                <ol className='text-left my-1'>
                                    <li>Pastikan anda sudah berada dalam suasana yang kondusif untuk mengerjakan soal</li>
                                    <li>Pastikan perangkat yang anda gunakan dapat digunakan dengan lancar hingga akhir psikotes</li>
                                    <li>Pastikan perangkat anda tersambung ke jaringan internet dan pastikan anda memiliki jaringan yang stabil</li>
                                    <li>Terdapat dua macam tes, yaitu Tes Intelegensi (IST) dan Tes Minat (RMIB) yang akan dikerjakan secara berurutan</li>
                                    <li>
                                        Jika terjadi masalah teknis selama mengerjakan soal, jawaban sementara anda 
                                        tetap akan tersimpan selama anda masih menggunakan perangkat yang sama
                                    </li>
                                    <li>Hubungi admin jika masih terdapat hal-hal yang ingin ditanyakan</li>
                                </ol>
                            </h6>
                        </Collapse>
                        <Collapse isOpen={ist}>
                            <Card>
                                <CardHeader className="bg-transparent border-bottom-0 text-center" style={{position:'relative'}}>
                                    <Button onClick={toggle} className="btn btn-sm bg-transparent border-0" style={{position:'absolute', top:'5px', left:'5px'}}>
                                        <i className="fa fa-angle-left mr-2">&nbsp;&nbsp;Back</i>
                                    </Button>
                                    <b>Petunjuk Pengerjaan Test IST</b>
                                </CardHeader>
                                <CardBody className="text-left">
                                    <ol className='text-left my-1'>
                                        <li>Pada Tes pertama, terdapat 9 macam sub tes yang akan dikerjakan secara berurutan</li>
                                        {/* <li>Waktu pengerjaan masing-masing subtes adalah enam(6) menit</li> */}
                                        <li>Usahakan anda dapat menjawab semua soal dengan tepat dan benar</li>
                                        {/* <li>Jika dalam suatu subtes waktu 6 menit telah berlalu, maka subtes tersebut akan dianggap selesai dan anda akan diarahkan ke subtes berikutnya</li>
                                        <li>Jika anda sudah menyelesaikan sebelum waktunya selesai, anda tidak dapat mengerjakan subtes sebelumnya</li> */}
                                    </ol>
                                </CardBody>
                            </Card>
                        </Collapse>
                        </>
                        }
                    </div>
                    {(!dataUser?.ist && !dataUser?.rmib) &&
                        <div className='mt-3 text-center'>
                            {ist ? <Link to="/ist" className="btn btn-main-rounded">Mulai Tes</Link> :
                                <Button color="main-rounded" onClick={toggle}>
                                    Petunjuk Tes IST
                                </Button>
                            }
                        </div>
                    }
                </CardBody>
            </Card>
        </div>
    )
}

export default Dashboard