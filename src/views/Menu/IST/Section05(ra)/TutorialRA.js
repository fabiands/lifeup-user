import React, {useState, useEffect} from 'react'
import { Button, Card, CardBody, CustomInput, Spinner } from 'reactstrap'
import request from "../../../../utils/request"
import TestRA from './TestRA'

function TutorialRA({submit}){
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [test, setTest] = useState(false)
    const [contain, setContain] = useState(false)
    const [correct, setCorrect] = useState([])

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
        setLoading(true)
        request.get('/ist/ra')
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
            .finally(() => setLoading(false))
    },[])

    const handleChange = (value, checked) => {
        const a = [0,6]
        let num = correct ?? []

        if(checked){
            num.push(value)
        }
        else if(!checked){
            let set = new Set(num)
            set.delete(value)
            num = Array.from(set)
        }
        
        if(num.length === 2){
            if(a.every(i => num.includes(i))){
                setContain(true)
            }
            else {
                setContain(false)
            }
        }
        else {
            setContain(false)
        }

        setCorrect(num)
    }

    const startRA = () => {
        setTest(true)
    }

    return(
        <Card className="bg-white">
            <CardBody className="p-4">
            {!test ?
                <>
                    <HintRA handleChange={handleChange} isModal={false} />
                    <Button color="main-rounded"
                        onClick={startRA}
                        disabled={loading || error || !contain}
                    >
                        {loading ? <Spinner color="light" size="sm" /> : error ? 'Error' : 'Mulai Tes'}
                    </Button>
                    {loading ? <small className='text-muted'><br />Mempersiapkan soal...</small> :
                    error ? <small className='text-danger error-reload' onClick={() => window.location.reload()}><br />Error, silahkan muat ulang</small> : null
                    }
                </>
            :
            <TestRA start={true} data={data} submit={submit} />
            }
        </CardBody>
        </Card>
    )
}

export const HintRA = ({handleChange, isModal, toggle}) => {
    const width = isModal ? 'w-100' : 'w-75'

    return(
        <div className={isModal ? 'px-3' : ''}>
            <div className='text-center'>
                <h3 className="mt-3"><b>Section RA</b></h3>
                <h4>
                    Petunjuk dan Contoh untuk Kelompok Soal 05<br />
                    (Soal-soal No. 77-96)
                </h4>
            </div>
            <div className={`${width} my-5 mx-auto text-left`}>
                Persoalan berikutnya ialah-soal-soal hitungan.
            </div>
            <div className={`${width} mt-3 mb-3 mx-auto text-left`}>
                <b className='my-2'>Contoh 05</b><br />
                <div className='mt-4'>
                    Sebatang pensil harganya 25 rupiah. Berapa harga 3 batang pensil ?<br />
                    <br />
                    Jawabannya ialah : 75
                    <br /><br />
                    Perhatikan cara menjawab soal tes berikut !
                </div>
                <div className="mt-3">
                    Kolom di bawah ini terdiri atas angka-angka 1 sampai 9 dan 0.<br />
                    Untuk menunjukkan jawaban suatu soal, maka pilihlah angka-angka yang terdapat 
                    di dalam jawaban itu.<br />
                    Keurutan angka jawaban tidak perlu dihiraukan.<br /><br />
                    Pada contoh 05, jawabannya ialah 75.<br />
                    Oleh karena itu, angka 5 dan 7 harus dipilih.
                </div>
                <div className="d-flex justify-content-between my-2 w-100">
                    <CustomInput type="checkbox" name="example_ra1" id="ex_ra1" value="" disabled={true} label={1} />
                    <CustomInput type="checkbox" name="example_ra1" id="ex_ra1" value="" disabled={true} label={2} />
                    <CustomInput type="checkbox" name="example_ra1" id="ex_ra1" value="" disabled={true} label={3} />
                    <CustomInput type="checkbox" name="example_ra1" id="ex_ra1" value="" disabled={true} label={4} />
                    <CustomInput type="checkbox" name="example_ra1" id="ex_ra1" value="" disabled={true} checked={true} label={5} />
                    <CustomInput type="checkbox" name="example_ra1" id="ex_ra1" value="" disabled={true} label={6} />
                    <CustomInput type="checkbox" name="example_ra1" id="ex_ra1" value="" disabled={true} checked={true} label={7} />
                    <CustomInput type="checkbox" name="example_ra1" id="ex_ra1" value="" disabled={true} label={8} />
                    <CustomInput type="checkbox" name="example_ra1" id="ex_ra1" value="" disabled={true} label={9} />
                    <CustomInput type="checkbox" name="example_ra1" id="ex_ra1" value="" disabled={true} label={0} />
                </div>
            </div>
            <div className={`${width} my-5 mx-auto text-left`}>
                <b className='my-2'>Contoh lain :</b>
                <div className='mt-2'>
                    Dengan sepeda Husin dapat mencapai 15 km dalam waktu 1 jam. Berapa km-kah yang dapat 
                    ia capai dalam waktu 4 jam ?
                    <br /><br />
                    Jawabannya ialah : 60
                    <br />
                    Maka untuk menunjukkan jawaban itu, angka 6 dan 0 harus dipilih
                </div>
                <div className="d-flex justify-content-between mt-2 mb-5 w-100">
                    <CustomInput type="checkbox" onChange={(e) => handleChange(1, e.target.checked)} name="example_ra2" id="example_ra1" value={1} label={1} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(2, e.target.checked)} name="example_ra2" id="example_ra2" value={2} label={2} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(3, e.target.checked)} name="example_ra2" id="example_ra3" value={3} label={3} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(4, e.target.checked)} name="example_ra2" id="example_ra4" value={4} label={4} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(5, e.target.checked)} name="example_ra2" id="example_ra5" value={5} label={5} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(6, e.target.checked)} name="example_ra2" id="example_ra6" value={6} label={6} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(7, e.target.checked)} name="example_ra2" id="example_ra7" value={7} label={7} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(8, e.target.checked)} name="example_ra2" id="example_ra8" value={8} label={8} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(9, e.target.checked)} name="example_ra2" id="example_ra9" value={9} label={9} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(0, e.target.checked)} name="example_ra2" id="example_ra0" value={0} label={0} />
                </div>
            </div>
            {isModal &&
                <div className='text-center mt-5 mb-3'>
                    <Button color="main-rounded" onClick={toggle}>Mengerti</Button>
                </div>
            }
        </div>
    )
}

export default TutorialRA