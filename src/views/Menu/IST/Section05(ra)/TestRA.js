import React, {useState, useEffect} from 'react'
import { Col, CustomInput, Row, Button, Card, CardBody, Modal, ModalBody } from 'reactstrap'
import TestTimer from '../../../../components/TestTimer'
import { HintRA } from './TutorialRA'

function TestRA({start, data, submit}){
    const local = localStorage.getItem('answers')
    const [timer, setTimer] = useState(false)
    const parse = JSON.parse(local)?.ra ?? []
    const [answer, setAnswer] = useState({})
    const [hint, setHint] = useState(false)

    useEffect(() => {
        if(parse.length > 0){
            let x = {}
            for(let i = 0; i < parse.length; i++){
                x = {...x, [i]: parse[i].split("")}
            }
            setAnswer(x)
        }
        // eslint-disable-next-line
    }, [])

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
    }, [])

    useEffect(() => {
        if(start){
            setTimer(true)
        }
    },[start])

    const handleChange = (id, value, checked) => {
        let num = answer[id] ?? []
        let sort = []
        if(checked){
            num.push(value)
            sort = num?.sort(function (a, b) {  return a - b;  });
        }
        else if(!checked){
            let set = new Set(num)
            set.delete(value)
            num = Array.from(set)
            sort = num?.sort(function (a, b) {  return a - b;  });
        }
        if(sort.includes('0')){
            sort.push(sort.shift())
        }
        const newAnswer = { ...answer, [id]: sort }
        setAnswer(newAnswer)
    }

    const handleSubmit = () => {
        const section = 'ra'
        let arr = []
        for(let i=0; i < 20; i++){
            if(answer[i]){
                arr.push(answer[i])
            }
            else{
                arr.push("")
            }
        }
        const merge = arr.map(item => {
            let i = ""
            i += item.toString()
            let noComma = i.replace(/,/g, '');
            return noComma
        })
        const parse = JSON.parse(local)
        let newAnswers = {...parse, [section]: merge}
        localStorage.setItem('answers', JSON.stringify(newAnswers))
        submit()
    }

    return(
        <div className='test-page'>
            {timer &&
                <TestTimer
                    test={true}
                    name="re"
                    timeStart={360}
                    onSubmit={handleSubmit}
                />
            }
            {data?.length > 0 && data.map((item, idx) => 
            <Card className="rounded shadow-sm my-4 mx-auto" key={idx} style={{width:'85%'}}>
                <CardBody>
                    <Row className="p-3">
                        <Col xs="12" className="text-left mb-1">{item.id+76}. {item.question}</Col>
                        <div className="ml-2 d-flex justify-content-between mt-1 w-100 mb-2">
                        {item.answers.map((ans, index) =>
                            <div key={index} className="py-2 px-3 text-left" style={{width:'10%'}}>
                                <CustomInput
                                    type="checkbox"
                                    id={`RE_${item.id}_${ans.value}`}
                                    name={`RE_${item.id}_${ans.value}`}
                                    label={ans.label}
                                    value={ans.value}
                                    checked={answer[idx]?.includes(ans.value) ?? false}
                                    onChange={(e) => handleChange(idx, ans.value, e.target.checked)}
                                />
                            </div>
                        )}
                        </div>
                    </Row>
                </CardBody>
            </Card>
            )}
            <Button color="main-rounded" className="my-3" onClick={handleSubmit}>Submit</Button>
            <div className='btn-hint'>
                <img src={require('../../../../assets/icon-hint.png')} alt="hint" className='scale-div' width={50} height={50}
                    onClick={() => setHint(true)}
                />
            </div>
            <Modal className='modal-hint' isOpen={hint} size="lg" toggle={() => setHint(false)} style={{borderRadius:'15px'}}>
                <ModalBody className='px-5'>
                    <HintRA isModal={true} handleChange={(val, checked) => console.log(val, checked)} toggle={() => setHint(false)} />
                </ModalBody>
            </Modal>
        </div>
    )
}

export default TestRA