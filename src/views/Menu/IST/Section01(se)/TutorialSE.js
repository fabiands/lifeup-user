import React, {useState, useEffect} from 'react'
import { Button, Card, CardBody, CustomInput, Spinner } from 'reactstrap'
import request from "../../../../utils/request"
import TestSE from './TestSE'

function TutorialSE({submit}){
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [test, setTest] = useState(false)
    const [correct, setCorrect] = useState(false)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
        setLoading(true)
        request.get('/ist/se')
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
            .finally(() => setLoading(false))
    },[])

    const startSE = () => {
        setTest(true)
    }


    return(
        <Card className="bg-white">
            <CardBody className="p-4">
            {!test ?
                <>
                    <HintSE setCorrect={setCorrect} isModal={false} />
                    <Button color="main-rounded" onClick={startSE} disabled={loading || error || !correct}>
                        {loading ? <Spinner color="light" size="sm" /> : error ? 'Error' : 'Mulai Tes'}
                    </Button>
                    {loading ? <small className='text-muted'><br />Mempersiapkan soal...</small> :
                    error ? <small className='text-danger error-reload' onClick={() => window.location.reload()}><br />Error, silahkan muat ulang</small> : null
                    }
                </>
            :
            <TestSE start={true} data={data} submit={submit} />
            }
        </CardBody>
        </Card>
    )
}

export const HintSE = ({setCorrect, isModal}) => {
    const width = isModal ? 'w-100' : 'w-75'
    return(
        <div className={isModal ? 'px-3' : ''}>
            <div className='text-center'>
                <h3 className="mt-3"><b>Section SE</b></h3>
                <h4>
                    Petunjuk dan Contoh untuk Kelompok Soal 01<br />
                    (Soal-soal No. 01-20)
                </h4>
            </div>
            <div className={`${width} my-5 mx-auto text-left`}>
                Soal-soal 01-20 terdiri atas kalimat-kalimat.<br />
                Pada setiap kalimat, satu kata hilang dan disediakan 5 (lima) kata pilihan sebagai penggantinya.
                <br />
                Pilihlah kata yang tepat yang dapat menyempurnakan kalimat itu !
            </div>
            <div className={`${width} mt-4 mb-3 mx-auto text-left`}>
                <b className='my-2'>Contoh 01</b><br />
                Seekor kuda mempunyai kesamaan terbanyak dengan seekor...........<br />
                <div className={`d-flex justify-content-between ${width} mb-4`}>
                    <CustomInput type="radio" id="example_se" value="" disabled={true} label="a. kucing" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_se" value="" disabled={true} label="b. bajing" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_se" value="" disabled={true} label="c. keledai" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_se" value="" disabled={true} label="d. lembu" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_se" value="" disabled={true} label="e. anjing" style={{width:'20%'}} />
                </div>
                <div className='mt-4'>
                    Jawaban yang benar ialah : <b>Keledai</b><br />
                    Oleh karena itu, pilihan <b>c. Keledai</b> harus dipilih<br />
                </div>
            </div>
            <div className={`${width} mt-2 mb-5 text-center mx-auto d-flex justify-content-between`}>
                <CustomInput type="radio" id="example_se" value="" disabled={true} label="a. kucing" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_se" value="" disabled={true} label="b. bajing" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_se" value="" disabled={true} checked={true} label="c. keledai" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_se" value="" disabled={true} label="d. lembu" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_se" value="" disabled={true} label="e. anjing" style={{width:'20%'}} />
            </div>
            <div className={`${width} mt-5 mb-3 mx-auto text-left`}>
                <b className='my-2'>Contoh berikutnya :</b><br />
                Lawan "harapan" ialah...........<br />
                {!isModal ?
                    <div className={`d-flex justify-content-between ${width} mt-3 mb-5`}>
                        <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_se_1" name="ex" value="" label="a. duka" style={{width:'20%'}} />
                        <CustomInput onClick={() => setCorrect(true)} type="radio" id="example_se_2" name="ex" value="" label="b. putus asa" style={{width:'20%'}} />
                        <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_se_3" name="ex" value="" label="c. sengsara" style={{width:'20%'}} />
                        <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_se_4" name="ex" value="" label="d. cinta" style={{width:'20%'}} />
                        <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_se_5" name="ex" value="" label="e. benci" style={{width:'20%'}} />
                    </div>
                :
                    <div className={`d-flex justify-content-between ${width} mt-3 mb-5`}>
                        <CustomInput type="radio" id="example_se_1" name="ex" value="" label="a. duka" style={{width:'20%'}} />
                        <CustomInput type="radio" id="example_se_2" name="ex" value="" label="b. putus asa" style={{width:'20%'}} />
                        <CustomInput type="radio" id="example_se_3" name="ex" value="" label="c. sengsara" style={{width:'20%'}} />
                        <CustomInput type="radio" id="example_se_4" name="ex" value="" label="d. cinta" style={{width:'20%'}} />
                        <CustomInput type="radio" id="example_se_5" name="ex" value="" label="e. benci" style={{width:'20%'}} />
                    </div>
                }
                Jawaban yang benar ialah : <b>Putus asa</b><br />
                Oleh karena itu, pilihan <b>b. Putus asa</b> harus dipilih<br />
            </div>
            {isModal &&
                <div className='text-center mt-5 mb-3'>
                    <Button color="main-rounded" onClick={() => setCorrect(false)}>Mengerti</Button>
                </div>
            }
        </div>
    )
}

export default TutorialSE