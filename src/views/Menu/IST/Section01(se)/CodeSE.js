import React, { useState } from 'react'
import { toast } from 'react-toastify';
import {Button, Card, CardBody, Col, Input, Row, Spinner} from 'reactstrap'
import { useAuthUser } from '../../../../store';
import TutorialSE from './TutorialSE';

function CodeSE({submit}){
    const user = useAuthUser();
    const [success, setSuccess] = useState(false)
    const [code, setCode] = useState('')
    const [section, setSection] = useState('')
    const [loading, setLoading] = useState(false)
    const [wrong, setWrong] = useState(0)

    const handleSubmit = () => {
        setLoading(true)
        if(code === user.room?.code && section === 'se1'){
            setSuccess(true)
        }
        else {
            toast.error('Kode yang anda masukkan salah')
            setWrong(wrong+1)
            setLoading(false)
            return;
        }
    }

    if(!success){
        return(
            <div className="page-content">
                <Card>
                    <CardBody className="p-5 text-center">
                        <h4>Masukkan Kode Section</h4>
                        <h6 className='mt-3 w-75 mx-auto'>
                            Untuk dapat melanjutkan ke tahap tes selanjutnya, anda harus memasukkan kode yang sesuai dengan 
                            kode kelas anda dan kode section setiap tes. Mintalah admin untuk memberikan kode tes untuk setiap section.
                        </h6>
                        <br />
                        <Row className='w-75 mx-auto d-flex justify-content-center'>
                            <Col xs='5'>
                                <Input
                                    name='code'
                                    id='code'
                                    value={code}
                                    onChange={(e) => setCode(e.target.value)}
                                />
                            </Col>
                            <Col xs='1' className='pt-1'>&#8212;</Col>
                            <Col xs='5'>
                                <Input
                                    name='section'
                                    id='section'
                                    value={section}
                                    onChange={(e) => setSection(e.target.value)}
                                />
                            </Col>
                            <Col xs='12' className='mt-4'>
                                <div className='d-flex justify-content-center py-2'>
                                    <Button color='main-rounded' disabled={!code || !section || loading} onClick={handleSubmit}>
                                        {loading ? <Spinner color='light' size='sm' /> : 'Submit'}
                                    </Button>
                                    
                                </div>
                            </Col>
                            {wrong >= 3 &&
                                <Col xs='12' className='text-center'>
                                    <small className='text-danger'>
                                        Periksa kembali penggunaan huruf besar, huruf kecil, dan angka. Hubungi 
                                        Admin atau petugas jika anda mengalami error terus-menerus, atau muat ulang 
                                        halaman anda.
                                    </small>
                                </Col>
                            }
                        </Row>
                    </CardBody>
                </Card>
            </div>
        )
    }
    else if(success){
        return <TutorialSE submit={submit} />
    }
}

export default CodeSE