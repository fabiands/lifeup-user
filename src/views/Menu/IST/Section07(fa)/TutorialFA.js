import React, {useState, useEffect} from 'react'
import { ReactSVG } from 'react-svg'
import { Button, Card, CardBody, CustomInput, Spinner } from 'reactstrap'
import request from "../../../../utils/request"
import TestFA from './TestFA'

const exAnswer = ['a','b','c','d','e']
const exQuestion = [1,2,3,4]

function TutorialFA({submit}){
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [test, setTest] = useState(false)
    const [correct, setCorrect] = useState(false)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
        setLoading(true)
        request.get('/ist/fa')
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
            .finally(() => setLoading(false))
    },[])

    const startFA = () => {
        setTest(true)
    }

    const handleChange = (ans) => {
        if(ans === 'e'){
            setCorrect(true)
        }
        else {
            setCorrect(false)
        }
    }

    return(
        <Card className="bg-white">
            <CardBody className="p-4">
            {!test ?
                <>
                    <HintFA handleChange={handleChange} isModal={false} />
                    <Button color="main-rounded" onClick={startFA} disabled={loading || error || !correct}>
                        {loading ? <Spinner color="light" size="sm" /> : error ? 'Error' : 'Mulai Tes'}
                    </Button>
                    {loading ? <small className='text-muted'><br />Mempersiapkan soal...</small> :
                    error ? <small className='text-danger error-reload' onClick={() => window.location.reload()}><br />Error, silahkan muat ulang</small> : null
                    }
                </>
            :
            <TestFA start={true} data={data} submit={submit} />
            }
        </CardBody>
        </Card>
    )
}

export const HintFA = ({handleChange, isModal, toggle}) => {
    const width = isModal ? 'w-100' : 'w-75'

    return(
        <div className={isModal ? 'px-3' : ''}>
            <div className='text-center'>
                <h3 className="mt-3"><b>Section FA</b></h3>
                <h4>
                    Petunjuk dan Contoh untuk Kelompok Soal 07<br />
                    (Soal-soal No. 117-136)
                </h4>
            </div>
            <div className={`${width} mt-5 mb-3 mx-auto text-left`}>
                Pada persoalan berikutnya, setiap soal memperlihatkan suatu bentuk yang terpotong menjadi beberapa bagian. <br />
                Carilah diantara bentuk-bentuk yang ditentukan (a,b,c,d,e) bentuk yang dapat dibangun dengan cara menyusun 
                potongan-potongan itu sedemikian rupa sehingga tidak ada kelebihan sudut atau ruang diantaranya.<br />
                <br />Carilah bentuk-bentuk tersebut dan pilihlah huruf yang menunjukkan satuan gambar tersebut
            </div>
            <div className={`${width} my-5 d-flex justify-content-between mx-auto`}>
                {exAnswer.map((item, idx) =>
                    <div key={idx} className='d-flex flex-column justify-content-center align-items-center' style={{width:'20%'}}>
                        <div className="d-flex justify-content-center align-items-center" style={{height:'80%'}}>
                            <ReactSVG
                                src={require(`../../../../assets/FA-01/example/ans_${item}.svg`)}
                                alt={`fa_example_${item}`}
                                style={{
                                    objectFit:'contain',
                                    width:'90%'
                                }}
                            />
                        </div>
                        <h4 className='mt-2'>
                            {item}
                        </h4>
                    </div>
                )}
            </div>
            <div className={`${width} mx-auto text-center mb-3`}><b>Contoh Soal :</b></div>
            <div style={{width:'90%'}} className='d-flex justify-content-between mx-auto'>
                {exQuestion.map((item, idx) =>
                    <div key={idx} className='d-flex flex-column justify-content-center align-items-center' style={{width:'25%'}}>
                        <div className="d-flex justify-content-center align-items-center mb-1" style={{height:'80%'}}>
                            <ReactSVG
                                src={require(`../../../../assets/FA-01/example/ex_${item}.svg`)}
                                style={{
                                    objectFit:'contain',
                                    width:'90%'
                                }}
                            />
                        </div>
                        <div className='d-flex justify-content-between mt-1' style={{width:'80%'}}>
                            {exAnswer.map((ans, index) =>
                                <div key={index} className="pl-1 text-left" style={{width:'20%'}}>
                                    {idx === 0 ?
                                    <CustomInput
                                        type="radio"
                                        id={`FA_${idx}_${ans}_${index}`}
                                        name={`FA_${idx}`}
                                        label={ans}
                                        checked={ans === 'a'}
                                        disabled={true}
                                        value={ans}
                                    />
                                    :
                                    idx === 1 ?
                                    <CustomInput
                                        type="radio"
                                        id={`FA_${idx}_${ans}_${index}`}
                                        name={`FA_${idx}`}
                                        label={ans}
                                        onChange={() => handleChange(ans)}
                                        value={ans}
                                    />
                                    :
                                    <CustomInput
                                        type="radio"
                                        id={`FA_${idx}_${ans}_${index}`}
                                        name={`FA_${idx}`}
                                        label={ans}
                                        value={ans}
                                    />
                                }
                                </div>
                            )}
                        </div>
                    </div>
                )}
            </div>
            <div className={`my-5 ${width} text-left mx-auto`}>
                Pada contoh soal, jika susunan pada gambar pertama digabungkan, maka akan menghasilkan bentuk a. <br />
                Oleh karena itu, pilihan <b>a</b> harus dipilih.<br /><br />
                Pada gambar kedua, susunan bentuk tersebut akan menghasilkan bentuk e, sehingga pilihan <b>e</b> harus dipilih.
            </div>
            {isModal &&
                <div className='text-center mt-5 mb-3'>
                    <Button color="main-rounded" onClick={toggle}>Mengerti</Button>
                </div>
            }
        </div>
    )
}

export default TutorialFA