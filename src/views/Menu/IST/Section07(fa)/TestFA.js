import React, {useState, useEffect} from 'react'
import { ReactSVG } from 'react-svg'
import { Col, CustomInput, Row, Button, Modal, ModalBody } from 'reactstrap'
import TestTimer from '../../../../components/TestTimer'
import { HintFA } from './TutorialFA'

function TestFA({start, data, submit}){
    const local = localStorage.getItem('answers')
    const [timer, setTimer] = useState(false)
    const [answer, setAnswer] = useState(JSON.parse(local)?.fa ?? [])
    const [section, setSection] = useState(1)
    const options1 = Object.keys(data[0]?.answers) ?? []
    const options2 = Object.keys(data[1]?.answers) ?? []
    const [hint, setHint] = useState(false)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
    },[])

    useEffect(() => {
        if(start){
            setTimer(true)
        }
    },[start])

    const handleChange = (id, value) => {
        const newAnswer = { ...answer, [id]: value }
        setAnswer(newAnswer)
    }

    const handleSubmit = () => {
        const section = 'fa'
        let arr = []
        for(let i=0; i < 20; i++){
            if(answer[i]){
                arr.push(answer[i])
            }
            else{
                arr.push("")
            }
        }
        const parse = JSON.parse(local)
        let newAnswers = {...parse, [section]: arr}
        localStorage.setItem('answers', JSON.stringify(newAnswers))
        submit()
        
    }

    return(
        <div className='test-page'>
            {timer &&
                <TestTimer
                    test={true}
                    name="fa"
                    timeStart={360}
                    onSubmit={handleSubmit}
                />
            }
            {section === 1 ?
            <SectionTest dex={0} answer={answer} handleChange={handleChange} section={1} data={data[0]} options={options1} nexButton={() => setSection(2)} />
            :
            <SectionTest dex={12} answer={answer} handleChange={handleChange} section={2} data={data[1]} options={options2} nexButton={handleSubmit} prevButton={() => setSection(1)} />
            }
            <div className='btn-hint-small'>
                <img src={require('../../../../assets/icon-hint.png')} alt="hint" className='scale-div' width={50} height={50}
                    onClick={() => setHint(true)}
                />
            </div>
            <Modal className='modal-hint' isOpen={hint} size="lg" toggle={() => setHint(false)} style={{borderRadius:'15px'}}>
                <ModalBody className='px-5'>
                    <HintFA isModal={true} handleChange={(ans) => console.log(ans)} toggle={() => setHint(false)} />
                </ModalBody>
            </Modal>
        </div>
    )
}

const SectionTest = ({section, data, options, nexButton, prevButton, handleChange, answer, dex}) => {
    return(
        <>
            <div className='fixed-options p-3 d-flex flex-column justify-content-between'>
                {options?.map((item, idx) => 
                    <div key={idx+dex} style={{height:'20%', width:'100%'}} className='d-flex flex-row justify-content-center align-items-center'>
                        <div className='d-flex justify-content-center align-items-center'>
                            <ReactSVG
                                src={require(`../../../../assets/FA-0${section}/options/fa_${section}_${item}.svg`)}
                                style={{
                                    objectFit:'contain',
                                    width:'90%'
                                }}
                            />
                        </div>
                        <h4 className='mr-2'>
                            {item}
                        </h4>
                    </div>
                )}
            </div>
            <Row className="my-3" style={{width:'95%'}}>
                {data?.questions.map((item, idx) =>
                <Col xs="4" key={idx+dex} className="p-1 my-1">
                    {item.id+dex+116}.
                    <ReactSVG
                        src={require(`../../../../assets/FA-0${section}/questions/${item.id+116}.svg`)}
                        style={{
                            objectFit:'contain',
                            width:'90%'
                        }}
                    />
                    <div className='d-flex justify-content-center'>
                        <div className="d-flex justify-content-between mt-1 w-75 mx-auto">
                            {options?.map((ans, index) =>
                                <div key={index+dex} className="p-1 text-left" style={{width:'20%'}}> 
                                    <CustomInput
                                        type="radio"
                                        id={`FA_${item.id}_${ans}`}
                                        name={`FA_${item.id}_${ans}`}
                                        label={ans}
                                        value={ans}
                                        checked={answer[idx+dex] === ans}
                                        onChange={() => handleChange(idx+dex, ans)}
                                    />
                                </div>
                            )}
                        </div>
                    </div>
                </Col>
                )}
            </Row>
            <div className="text-center">
                {section === 2 && <Button color="outline-rounded" className="my-3 mr-3" onClick={prevButton}>Sebelumnya</Button>}
                <Button color="main-rounded" className={`my-3 ${section === 2 ? `ml-3` : ``}`} onClick={nexButton}>
                    {section === 1 ? 'Selanjutnya' : 'Submit'}
                </Button>
            </div>
        </>
    )
}

export default TestFA