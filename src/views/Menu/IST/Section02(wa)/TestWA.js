import React, { useState, useEffect } from 'react'
import { Col, CustomInput, Row, Button, Card, CardBody, Modal, ModalBody } from 'reactstrap'
import TestTimer from '../../../../components/TestTimer'
import {HintWA} from './TutorialWA'

function TestWA({ start, data, submit }) {
    const local = localStorage.getItem('answers')
    const [timer, setTimer] = useState(false)
    const [hint, setHint] = useState(false)
    const [answer, setAnswer] = useState(JSON.parse(local)?.wa ?? [])

    useEffect(() => {
        if (start) {
            setTimer(true)
        }
    }, [start])

    useEffect(() => {
        window.scrollTo({ top: 0, behavior: 'smooth' })
    }, [])

    const handleChange = (id, value) => {
        const newAnswer = { ...answer, [id]: value }
        setAnswer(newAnswer)
    }

    const handleSubmit = () => {
        const section = 'wa'
        let arr = []
        for (let i = 0; i < 20; i++) {
            if (answer[i]) {
                arr.push(answer[i])
            }
            else {
                arr.push("")
            }
        }
        const parse = JSON.parse(local)
        let newAnswers = { ...parse, [section]: arr }
        localStorage.setItem('answers', JSON.stringify(newAnswers))
        submit()

    }

    return (
        <div className='test-page'>
            {timer &&
                <TestTimer
                    test={true}
                    name="wa"
                    timeStart={360}
                    onSubmit={handleSubmit}
                />
            }
            {data?.length > 0 && data.map((item, idx) =>
            <Card className="rounded shadow-sm my-4 mx-auto" key={idx} style={{width:'85%'}}>
                <CardBody>
                    <Row className="p-3">
                        <Col xs="1" className="text-left pt-1">{item.id+20}.</Col>
                        <Col xs="11" className="d-flex justify-content-between px-0" style={{width:'100%'}}>
                        {item.answers.map((ans, index) =>
                            <div key={index} className="py-2 px-3 text-left" style={{width:'20%'}}>
                                <CustomInput
                                    type="radio"
                                    id={`WA_${item.id}_${ans.value}`}
                                    name={`WA_${item.id}_${ans.value}`}
                                    label={ans.value + ". " + ans.label}
                                    value={ans.value}
                                    checked={answer[idx] === ans.value}
                                    onChange={() => handleChange(idx, ans.value)}
                                />
                            </div>
                        )}
                        </Col>
                    </Row>
                </CardBody>
            </Card>
            )}
            <Button color="main-rounded" className="my-3" onClick={handleSubmit}>Submit</Button>
            <div className='btn-hint'>
                <img src={require('../../../../assets/icon-hint.png')} alt="hint" className='scale-div' width={50} height={50}
                    onClick={() => setHint(true)}
                />
            </div>
            <Modal className='modal-hint' isOpen={hint} size="lg" toggle={() => setHint(false)} style={{borderRadius:'15px'}}>
                <ModalBody className='px-5'>
                    <HintWA isModal={true} setCorrect={setHint} />
                </ModalBody>
            </Modal>
        </div>
    )
}

export default TestWA