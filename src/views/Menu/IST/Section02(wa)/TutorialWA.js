import React, {useState, useEffect} from 'react'
import { Button, Card, CardBody, CustomInput, Spinner } from 'reactstrap'
import request from "../../../../utils/request"
import TestWA from './TestWA'

function TutorialWA({submit}){
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [test, setTest] = useState(false)
    const [correct, setCorrect] = useState(false)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
        setLoading(true)
        request.get('/ist/wa')
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
            .finally(() => setLoading(false))
    },[])

    const startWA = () => {
        setTest(true)
    }


    return(
        <Card className="bg-white">
            <CardBody className="p-4">
            {!test ?
                <>
                    <HintWA setCorrect={setCorrect} isModal={false} />
                    <Button color="main-rounded" onClick={startWA} disabled={loading || error || !correct}>
                        {loading ? <Spinner color="light" size="sm" /> : error ? 'Error' : 'Mulai Tes'}
                    </Button>
                    {loading ? <small className='text-muted'><br />Mempersiapkan soal...</small> :
                    error ? <small className='text-danger error-reload' onClick={() => window.location.reload()}><br />Error, silahkan muat ulang</small> : null
                    }
                </>
            :
            <TestWA start={true} data={data} submit={submit} />
            }
        </CardBody>
        </Card>
    )
}

export const HintWA = ({setCorrect, isModal}) => {
    const width = isModal ? 'w-100' : 'w-75'
    return(
        <div className={isModal ? 'px-3' : ''}>
            <div className='text-center'>
                <h3 className="mt-3"><b>Section WA</b></h3>
                <h4>
                    Petunjuk dan Contoh untuk Kelompok Soal 02<br />
                    (Soal-soal No. 21-40)
                </h4>
            </div>
            <div className={`${width} my-5 mx-auto text-left`}>
                Ditentukan 5 kata.<br />
                Pada 4 dari 5 kata itu terdapat suatu kesamaan
                <br />
                Carilah kata kelima yang tidak memiliki kemampuan dengan keempat kata tersebut.
            </div>
            <div className={`${width} mt-4 mb-3 mx-auto text-left`}>
                <b className='my-2'>Contoh 02</b><br />
                <div className={`d-flex justify-content-between ${width} mb-4`}>
                    <CustomInput type="radio" id="example_wa" value="" disabled={true} label="a. meja" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_wa" value="" disabled={true} label="b. kursi" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_wa" value="" disabled={true} label="c. burung" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_wa" value="" disabled={true} label="d. lemari" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_wa" value="" disabled={true} label="e. tempat tidur" style={{width:'20%'}} />
                </div>
                <div className='mt-4'>
                    Meja, kursi, lemari, dan tempat tidur adalah perabot rumah (meubel)<br />
                    Burung tidak termasuk perabot rumah, atau tidak memiliki kesamaan terhadap keempat barang tersebut. <br />
                    Oleh karena itu, pilihan <b>c. burung</b> harus dipilih<br />
                </div>
            </div>
            <div className={`${width} mt-2 mb-5 text-center mx-auto d-flex justify-content-between`}>
                <CustomInput type="radio" id="example_wa" value="" disabled={true} label="a. meja" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_wa" value="" disabled={true} label="b. kursi" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_wa" value="" disabled={true} checked={true} label="c. burung" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_wa" value="" disabled={true} label="d. lemari" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_wa" value="" disabled={true} label="e. tempat tidur" style={{width:'20%'}} />
            </div>
            <div className={`${width} mt-5 mb-3 mx-auto text-left`}>
                <b className='my-2'>Contoh berikutnya :</b><br />
                <div className={`d-flex justify-content-between ${width} mt-3 mb-5`}>
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_wa_1" name="example_wa" value="" label="a. duduk" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_wa_2" name="example_wa" value="" label="b. berbaring" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_wa_3" name="example_wa" value="" label="c. berdiri" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(true)} type="radio" id="example_wa_4" name="example_wa" value="" label="d. berjalan" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_wa_5" name="example_wa" value="" label="e. berjongkok" style={{width:'20%'}} />
                </div>
                Duduk, berbaring, berdiri, dan berjongkok adalah kondisi ketika seseorang tidak bergerak atau berpindah dari tempatnya.
                Maka jawaban <b>d. berjalan</b> harus dipilih<br />
            </div>
            {isModal &&
                <div className='text-center mt-5 mb-3'>
                    <Button color="main-rounded" onClick={() => setCorrect(false)}>Mengerti</Button>
                </div>
            }
        </div>
    )
}

export default TutorialWA