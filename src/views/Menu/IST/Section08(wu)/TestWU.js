import React, {useState, useEffect} from 'react'
import { ReactSVG } from 'react-svg'
import { Col, CustomInput, Row, Button, Modal, ModalBody } from 'reactstrap'
import TestTimer from '../../../../components/TestTimer'
import { HintWU } from './TutorialWU'

function TestWU({start, data, submit}){
    const local = localStorage.getItem('answers')
    const [timer, setTimer] = useState(false)
    const [answer, setAnswer] = useState(JSON.parse(local)?.wu ?? [])
    const options = Object.keys(data?.answers) ?? []
    const [hint, setHint] = useState(false)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
    },[])

    useEffect(() => {
        if(start){
            setTimer(true)
        }
    },[start])

    const handleChange = (id, value) => {
        const newAnswer = { ...answer, [id]: value }
        setAnswer(newAnswer)
    }

    const handleSubmit = () => {
        const section = 'wu'
        let arr = []
        for(let i=0; i < 20; i++){
            if(answer[i]){
                arr.push(answer[i])
            }
            else{
                arr.push("")
            }
        }
        const parse = JSON.parse(local)
        let newAnswers = {...parse, [section]: arr}
        localStorage.setItem('answers', JSON.stringify(newAnswers))
        submit()
        
    }

    return(
        <div className='test-page'>
            {timer &&
                <TestTimer
                    test={true}
                    name="wu"
                    timeStart={360}
                    onSubmit={handleSubmit}
                />
            }
            <div className='fixed-options p-3 d-flex flex-column justify-content-between'>
                {options?.map((item, idx) => 
                    <div key={idx} style={{height:'20%', width:'100%'}} className='d-flex flex-row justify-content-center align-items-center'>
                        <div className='d-flex justify-content-center align-items-center'>
                            <ReactSVG
                                src={require(`../../../../assets/WU/options/wu_${item}.svg`)}
                                style={{
                                    objectFit:'contain',
                                    width:'90%'
                                }}
                            />
                        </div>
                        <h4 className='mr-2'>
                            {item}
                        </h4>
                    </div>
                )}
            </div>
            <Row className="my-3" style={{width:'95%'}}>
                {data?.questions.map((item, idx) =>
                <Col xs="4" key={idx} className="p-1 my-1">
                    <div className='d-flex align-items-center justify-content-center'>
                        <span>
                            {item.id+136}. 
                        </span>
                        <ReactSVG
                            src={require(`../../../../assets/WU/questions/${item.id+136}.svg`)}
                            style={{
                                objectFit:'contain',
                                width:'70%'
                            }}
                            />
                    </div>
                    <div className='d-flex justify-content-center'>
                        <div className="d-flex justify-content-between mt-1 w-75 mx-auto">
                            {options?.map((ans, index) =>
                                <div key={index} className="p-1 text-left" style={{width:'20%'}}> 
                                    <CustomInput
                                        type="radio"
                                        id={`wu_${item.id}_${ans}`}
                                        name={`wu_${item.id}_${ans}`}
                                        label={ans}
                                        value={ans}
                                        checked={answer[idx] === ans}
                                        onChange={() => handleChange(idx, ans)}
                                    />
                                </div>
                            )}
                        </div>
                    </div>
                </Col>
                )}
            </Row>
            <Button color="main-rounded" className="my-3" onClick={handleSubmit}>
                Submit
            </Button>
            <div className='btn-hint-small'>
                <img src={require('../../../../assets/icon-hint.png')} alt="hint" className='scale-div' width={50} height={50}
                    onClick={() => setHint(true)}
                />
            </div>
            <Modal className='modal-hint' isOpen={hint} size="lg" toggle={() => setHint(false)} style={{borderRadius:'15px'}}>
                <ModalBody className='px-5'>
                    <HintWU isModal={true} handleChange={(ans) => console.log(ans)} toggle={() => setHint(false)} />
                </ModalBody>
            </Modal>
        </div>
    )
}

export default TestWU