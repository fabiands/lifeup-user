import React, {useState, useEffect} from 'react'
import { ReactSVG } from 'react-svg'
import { Button, Card, CardBody, CustomInput, Spinner } from 'reactstrap'
import request from "../../../../utils/request"
import TestWU from './TestWU'

const exAnswer = ['a','b','c','d','e']
const exQuestion = [1,2,3,4,5]

function TutorialWU({submit}){
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [test, setTest] = useState(false)
    const [correct, setCorrect] = useState(false)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
        setLoading(true)
        request.get('/ist/wu')
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
            .finally(() => setLoading(false))
    },[])

    const startWU = () => {
        setTest(true)
    }

    const handleChange = (ans) => {
        if(ans === 'a'){
            setCorrect(true)
        }
        else {
            setCorrect(false)
        }
    }

    return(
        <Card className="bg-white">
            <CardBody className="p-4">
            {!test ?
                <>
                    <HintWU handleChange={handleChange} isModal={false} />
                    <Button color="main-rounded" onClick={startWU} disabled={loading || error || !correct}>
                        {loading ? <Spinner color="light" size="sm" /> : error ? 'Error' : 'Mulai Tes'}
                    </Button>
                    {loading ? <small className='text-muted'><br />Mempersiapkan soal...</small> :
                    error ? <small className='text-danger error-reload' onClick={() => window.location.reload()}><br />Error, silahkan muat ulang</small> : null
                    }
                </>
            :
            <TestWU start={true} data={data} submit={submit} />
            }
        </CardBody>
        </Card>
    )
}

export const HintWU = ({handleChange, isModal, toggle}) => {
    const width = isModal ? 'w-100' : 'w-75'

    return(
        <div>
            <div className='text-center'>
                <h3 className="mt-3"><b>Section WU</b></h3>
                <h4>
                    Petunjuk dan Contoh untuk Kelompok Soal 08<br />
                    (Soal-soal No. 137-156)
                </h4>
            </div>
            <div className={`${width} mt-5 mb-3 mx-auto text-left`}>
                Ditentukan lima buah kubus a, b, c, d, e. Pada tiap-tiap kubus terdapat enam tanda yang berlainan pada setiap sisinya. 
                Tiga dari tanda itu dapat dilihat.
                <br />
                Kubus-kubus yang ditentukan itu ialah kubus-kubus yang berbeda, artinya kubus-kubus itu dapat mempunyai 
                tanda-tanda yang sama, akan tetapi susunannya berlainan.
                <br />
                Setiap soal memperlihatkan salah satu kubus yang ditentukan di dalam kedudukan yang berbeda. Carilah kubus yang 
                dimaksudkan itu dan pilih jawaban kubus yang sesuai.<br />
                Kubus dapat diputar, dapat digulingkan atau diputar dan digulingkan dalam pikiran teman-teman, sehingga mungkin 
                akan terlihat suatu tanda yang baru.
            </div>

            <OptionAnswer width={width} example={true} handleChange={handleChange} />

            <div className={`${width} mx-auto text-left mb-2`}><b>Contoh Soal :</b></div>
            <div className={`${width} d-flex justify-content-between mx-auto`}>
                {exQuestion.map((item, idx) =>
                    <div key={idx} className='d-flex flex-column justify-content-center align-items-center' style={{width:'20%'}}>
                        <div className="d-flex justify-content-center align-items-center mb-1" style={{height:'80%'}}>
                            <ReactSVG
                                src={require(`../../../../assets/WU/example/ex_${item}.svg`)}
                                style={{
                                    objectFit:'contain',
                                    width:'90%'
                                }}
                            />
                        </div>
                        <h5 className='mt-2'>
                            ({item})
                        </h5>
                    </div>
                )}
            </div>
            <div className={`my-5 ${width} text-left mx-auto`}>
                Gambar 1 memperlihatkan kubus a dengan kedudukan yang berbeda.<br />
                Kubus 1 harus digulingkan ke kiri satu kali terlebih dahulu, kemudian diputar ke kiri satu kali 
                sehingga sisi kubus yang nampak akan sama seperti kubus a. Maka pada Contoh soal 1, kubus <b>a</b> atau pilihan <b>a</b> harus dipilih.
                
                {!isModal && <OptionAnswer width={width} example={false} handleChange={handleChange} />}

                Gambar 2 memperlihatkan kubus e.<br />
                Cara mendapatkannya adalah dengan digulingkan ke kiri satu kali dan diputar ke kiri satu kali sehingga 
                sisi kubus yang bertanda garis silang terletak di depan dan akan sama seperti kubus e.
                <br /><br />
                Gambar 3 memperlihatkan kubus b.<br />
                Cara mendapatkannya adalah dengan menggulingkannya ke kiri satu kali, sehingga dasar kubus yang tadinya tidak terlihat 
                memunculkan tanda baru (dalam hal ini adalah tanda dua segi empat hitam) dan tanda silang pada sisi atas kubus menjadi 
                tidak terlihat kembali.
                <br /><br />
                Gambar 4 memperlihatkan kubus c, dan Gambar 5 memperlihatkan kubus d.
            </div>
            {isModal &&
                <div className='text-center mt-5 mb-3'>
                    <Button color="main-rounded" onClick={toggle}>Mengerti</Button>
                </div>
            }
        </div>
    )
}

const OptionAnswer = ({example, width, handleChange}) => {

    return(
        <div className={`${width} my-5 d-flex justify-content-between mx-auto`}>
            {exAnswer.map((item, idx) =>
                <div key={idx} className='d-flex flex-column justify-content-center align-items-center' style={{width:'20%'}}>
                    <div className="d-flex justify-content-center align-items-center" style={{height:'80%'}}>
                        <ReactSVG
                            src={require(`../../../../assets/WU/example/ans_${item}.svg`)}
                            style={{
                                objectFit:'contain',
                                width:'90%'
                            }}
                        />
                    </div>
                    {example ?
                        <h4 className='mt-2'>
                            {item}
                        </h4>
                        :
                        <CustomInput
                            type="radio"
                            id={`WU_${idx}_${item}`}
                            name="WU_options"
                            label={item}
                            onChange={() => handleChange(item)}
                            value={item}
                        />
                    }
                </div>
            )}
        </div>
    )
}

export default TutorialWU