import React, {useState, useEffect} from 'react'
import { Button, Card, CardBody, CustomInput, Spinner } from 'reactstrap'
import request from "../../../../utils/request"
import TestAN from './TestAN'

function TutorialAN({submit}){
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [test, setTest] = useState(false)
    const [correct, setCorrect] = useState(false)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
        setLoading(true)
        request.get('/ist/an')
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
            .finally(() => setLoading(false))
    },[])

    const startAN = () => {
        setTest(true)
    }


    return(
        <Card className="bg-white">
            <CardBody className="p-4">
            {!test ?
                <>
                    <HintAN setCorrect={setCorrect} isModal={false} />
                    <Button color="main-rounded" onClick={startAN} disabled={loading || error || !correct}>
                        {loading ? <Spinner color="light" size="sm" /> : error ? 'Error' : 'Mulai Tes'}
                    </Button>
                    {loading ? <small className='text-muted'><br />Mempersiapkan soal...</small> :
                    error ? <small className='text-danger error-reload' onClick={() => window.location.reload()}><br />Error, silahkan muat ulang</small> : null
                    }
                </>
            :
            <TestAN start={true} data={data} submit={submit} />
            }
        </CardBody>
        </Card>
    )
}

export const HintAN = ({setCorrect, isModal}) => {
    const width = isModal ? 'w-100' : 'w-75'
    return(
        <div className={isModal ? 'px-3' : ''}>
            <div className='text-center'>
                <h3 className="mt-3"><b>Section AN</b></h3>
                <h4>
                    Petunjuk dan Contoh untuk Kelompok Soal 03<br />
                    (Soal-soal No. 41-60)
                </h4>
            </div>
            <div className={`${width} my-5 mx-auto text-left`}>
                Ditentukan 3 kata.<br />
                Kata pertama dan kata kedua memiliki suatu hubungan tertentu
                <br />
                Antara kata ketiga dan salah satu diantara lima pilihan juga harus memiliki hubungan yang sama.
                <br />
                Carilah kata itu.
            </div>
            <div className={`${width} mt-4 mb-3 mx-auto text-left`}>
                <b className='my-2'>Contoh 03</b><br />
                Hutan : pohon = tembok : ?
                <div className={`d-flex justify-content-between ${width} mb-4`}>
                    <CustomInput type="radio" id="example_an" value="" disabled={true} label="a. batu bata" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_an" value="" disabled={true} label="b. rumah" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_an" value="" disabled={true} label="c. semen" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_an" value="" disabled={true} label="d. putih" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_an" value="" disabled={true} label="e. dinding" style={{width:'20%'}} />
                </div>
                <div className='mt-4'>
                    Hubungan antara hutan dan pohon ialah bahwa hutan terdiri atas pohon-pohon, maka hubungan 
                    antara tembok dan salah satu kata pilihan ialah bahwa tembok terdiri atas batu bata. <br />
                    Oleh karena itu, pilihan <b>a. batu bata</b> harus dipilih<br />
                </div>
            </div>
            <div className={`${width} mt-2 mb-5 text-center mx-auto d-flex justify-content-between`}>
                <CustomInput type="radio" id="example_an" value="" disabled={true} checked={true} label="a. batu bata" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_an" value="" disabled={true} label="b. rumah" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_an" value="" disabled={true} label="c. semen" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_an" value="" disabled={true} label="d. putih" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_an" value="" disabled={true} label="e. dinding" style={{width:'20%'}} />
            </div>
            <div className={`${width} mt-5 mb-3 mx-auto text-left`}>
                <b className='my-2'>Contoh berikutnya :</b><br />
                Gelap : terang = basah : ? 
                <div className={`d-flex justify-content-between ${width} mt-3 mb-5`}>
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_an_1" name="ex_an" value="" label="a. hujan" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_an_2" name="ex_an" value="" label="b. hari" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_an_3" name="ex_an" value="" label="c. lembab" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_an_4" name="ex_an" value="" label="d. angin" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(true)} type="radio" id="example_an_5" name="ex_an" value="" label="e. kering" style={{width:'20%'}} />
                </div>
                Gelap ialah lawan dari terang, maka lawan dari basah ialah kering.
                Maka jawaban <b>e. kering</b> harus dipilih<br />
            </div>
            {isModal &&
                <div className='text-center mt-5 mb-3'>
                    <Button color="main-rounded" onClick={() => setCorrect(false)}>Mengerti</Button>
                </div>
            }
        </div>
    )
}

export default TutorialAN