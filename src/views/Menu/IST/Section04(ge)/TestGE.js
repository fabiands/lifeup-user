import React, {useState, useEffect} from 'react'
import { Col, Input, Row, Button, Card, CardBody, Modal, ModalBody } from 'reactstrap'
import TestTimer from '../../../../components/TestTimer'
import { HintGE } from './TutorialGE'

function TestGE({start, data, submit}){
    const local = localStorage.getItem('answers')
    const [timer, setTimer] = useState(false)
    const [answer, setAnswer] = useState(JSON.parse(local)?.ge ?? [])
    const [correct, setCorrect] = useState('')
    const [hint, setHint] = useState(false)

    useEffect(() => {
        if(start){
            setTimer(true)
        }
    },[start])

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
    }, [])

    const handleChange = (id, value) => {
        const newAnswer = { ...answer, [id]: value }
        setAnswer(newAnswer)
    }

    const handleSubmit = () => {
        const section = 'ge'
        let arr = []
        for(let i=0; i < 20; i++){
            if(answer[i]){
                arr.push(answer[i])
            }
            else{
                arr.push("")
            }
        }
        const parse = JSON.parse(local)
        let newAnswers = {...parse, [section]: arr}
        localStorage.setItem('answers', JSON.stringify(newAnswers))
        submit()
    }

    return(
        <div className='test-page'>
            {timer &&
                <TestTimer
                    test={true}
                    name="ge"
                    timeStart={360}
                    onSubmit={handleSubmit}
                />
            }
            {data?.length > 0 && data.map((item, idx) => 
            <Card className="rounded shadow-sm my-4 mx-auto" key={idx} style={{width:'85%'}}>
                <CardBody>
                    <Row className="p-3">
                        <Col xs="12" className="text-left mb-1">{item.id+60}. {item.question}</Col>
                        <Col xs="12" className="w-50 my-1 input_test">
                            <Input
                                name={`GE_${item.id}`}
                                id={`GE_${item.id}`}
                                value={answer[idx]}
                                onChange={(e) => handleChange(idx, e.target.value)}
                            />
                        </Col>
                    </Row>
                </CardBody>
            </Card>
            )}
            <Button color="main-rounded" className="my-3" onClick={handleSubmit}>Submit</Button>
            <div className='btn-hint'>
                <img src={require('../../../../assets/icon-hint.png')} alt="hint" className='scale-div' width={50} height={50}
                    onClick={() => setHint(true)}
                />
            </div>
            <Modal className='modal-hint' isOpen={hint} size="lg" toggle={() => setHint(false)} style={{borderRadius:'15px'}}>
                <ModalBody className='px-5'>
                    <HintGE isModal={true} toggle={() => setHint(false)} correct={correct} setCorrect={setCorrect} />
                </ModalBody>
            </Modal>
        </div>
    )
}

export default TestGE