import React, {useState, useEffect} from 'react'
import { Button, Card, CardBody, Label, Input, Spinner } from 'reactstrap'
import request from "../../../../utils/request"
import TestGE from './TestGE'

function TutorialGE({submit}){
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [test, setTest] = useState(false)
    const [correct, setCorrect] = useState('')

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
        setLoading(true)
        request.get('/ist/ge')
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
            .finally(() => setLoading(false))
    },[])

    const startGE = () => {
        setTest(true)
    }


    return(
        <Card className="bg-white">
            <CardBody className="p-4">
            {!test ?
                <>
                    <HintGE correct={correct} setCorrect={setCorrect} isModal={false} />
                    <Button color="main-rounded" onClick={startGE} disabled={loading || error || correct !== 'pakaian'}>
                        {loading ? <Spinner color="light" size="sm" /> : error ? 'Error' : 'Mulai Tes'}
                    </Button>
                    {loading ? <small className='text-muted'><br />Mempersiapkan soal...</small> :
                    error ? <small className='text-danger error-reload' onClick={() => window.location.reload()}><br />Error, silahkan muat ulang</small> : null
                    }
                </>
            :
                <TestGE start={true} data={data} submit={submit} />
            }
        </CardBody>
        </Card>
    )
}

export const HintGE = ({correct, setCorrect, isModal, toggle}) => {
    const width = isModal ? 'w-100' : 'w-75'
    return(
        <div className={isModal ? 'px-3' : ''}>
            <div className='text-center'>
                <h3 className="mt-3"><b>Section GE</b></h3>
                <h4>
                    Petunjuk dan Contoh untuk Kelompok Soal 04<br />
                    (Soal-soal No. 61-76)
                </h4>
            </div>
            <div className={`${width} my-5 mx-auto text-left`}>
                Ditentukan dua kata. Carilah satu kata yang meliputi pengertian kedua kata yang lain.
                <br />
                Tulislah kata tersebut pada Form Jawaban yang sudah disediakan.
            </div>
            <div className={`${width} mt-3 mb-3 mx-auto text-left`}>
                <b className='my-2'>Contoh 04</b><br />
                <Label htmlFor="example_an_1" className="input-label">Ayam - itik</Label>
                <Input id="example_an_1" name="example_an_1" className="w-50" value="" disabled={true} />
                <div className='mt-4'>
                    Perkataan "burung" dapat meliputi pengertian kedua kata tersebut. Maka jawabannya ialah "burung".<br />
                    Oleh karena itu, isilah <b>burung</b> pada form jawaban yang telah disediakan
                </div>
            </div>
            <div className={`${width} mt-4 mb-3 mx-auto text-left`}>
                <Label htmlFor="answers_an_1" className="input-label">Ayam - itik</Label>
                <Input id="answers_an_1" name="answers_an_1" className="w-50" value="burung" disabled={true} />
            </div>
            <div className={`${width} my-5 mx-auto text-left`}>
                <b className='my-2'>Contoh berikutnya :</b><br />
                <Label htmlFor="answers_an_2" className="input-label">Gaun - celana</Label>
                <Input id="answers_an_2" name="answers_an_2" className="w-50 mb-2" value={correct} onChange={(e) => setCorrect(e.target.value)} />
                <span className='mt-4'>
                    Pada contoh di atas, jawabannya ialah "pakaian", maka <b>pakaian</b> yang seharusnya ditulis.<br />
                    Carilah selalu perkataan yang tepat yang dapat meliputi pengertian kedua kata itu.
                </span>
            </div>
            {isModal &&
                <div className='text-center mt-5 mb-3'>
                    <Button color="main-rounded" onClick={toggle}>Mengerti</Button>
                </div>
            }
        </div>
    )
}

export default TutorialGE