export const words = [
    {
        parent:'BUNGA',
        child:'Soka - Larat - Flamboyan - Yasmin - Dahlia'
    },
    {
        parent:'PERKAKAS',
        child:'Wajan - Jarum - Kikir - Cangkul - Palu'
    },
    {
        parent:'BURUNG',
        child:'Itik - Elang - Walet - Tekukur - Nuri'
    },
    {
        parent:'KESENIAN',
        child:'Quintent - Arca - Opera - Gamelan - Ukiran'
    },
    {
        parent:'BINATANG',
        child:'Musang - Rusa - Beruang - Zebra - Harimau'
    },

]