import React, {useState, useEffect} from 'react'
import {Row, Col, Card, CardBody} from 'reactstrap'
import {words} from './words'
import TestTimer from '../../../../components/TestTimer'

function OpeningME({start, submit}){
    const [timer, setTimer] = useState(false)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
    },[])

    useEffect(() => {
        if(start){
            setTimer(true)
        }
    },[start])

    return(
        <div className='w-75 text-center mx-auto py-5'>
            {timer &&
                <TestTimer
                    test={true}
                    name="me"
                    timeStart={180}
                    onSubmit={submit}
                />
            }
            <span className='mb-3'>Disediakan waktu 3 menit untuk menghafalkan kata-kata di bawah ini</span>
            <Card className="my-3">
                <CardBody className="py-2 px-3">
                    {words.map((item, idx) =>
                        <Row key={idx} className="my-2" style={{fontSize:'1rem'}}>
                            <Col xs="3" className="text-left pl-4">{item.parent}</Col>
                            <Col ms="1" className="text-center">:</Col>
                            <Col xs="8" className="text-left pl-1">{item.child}</Col>
                        </Row>
                    )}
                </CardBody>
            </Card>
            
            {/* yg dibawah ini di comment yaa kl udh selesai */}
            {/* <div className='text-center w-100 mx-auto my-3'>
                Silahkan menunggu waktu atau tekan tombol <b>Selanjutnya</b> jika anda sudah 
                selesai menghafalkan kata-kata di atas.
                <div className="w-50 mx-auto text-center">
                    <button className="login-submit mt-4 mb-2 py-2" onClick={submit}>Selanjutnya</button>
                </div>
            </div> */}
        </div>
    )
}

export default OpeningME