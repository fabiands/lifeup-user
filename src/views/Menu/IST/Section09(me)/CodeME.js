import React, { useState } from 'react'
import { toast } from 'react-toastify';
import {Button, Card, CardBody, Col, Input, Row, Spinner, CustomInput} from 'reactstrap'
import { useAuthUser } from '../../../../store';
import TutorialME from './TutorialME';

function CodeME({submit}){
    const user = useAuthUser();
    const [step, setStep] = useState(0)
    const [code, setCode] = useState('')
    const [section, setSection] = useState('')
    const [loading, setLoading] = useState(false)
    const [wrong, setWrong] = useState(0)

    const handleSubmit = () => {
        setLoading(true)
        if(code === user.room?.code && section === 'me9'){
            setStep(1)
        }
        else {
            toast.error('Kode yang anda masukkan salah')
            setWrong(wrong+1)
            setLoading(false)
            return;
        }
    }

    if(step === 0){
        return(
            <div className="page-content">
                <Card>
                    <CardBody className="p-5 text-center">
                        <h4>Masukkan Kode Section</h4>
                        <h6 className='mt-3 w-75 mx-auto'>
                            Untuk dapat melanjutkan ke tahap tes selanjutnya, anda harus memasukkan kode yang sesuai dengan 
                            kode kelas anda dan kode section setiap tes. Mintalah admin untuk memberikan kode tes untuk setiap section.
                        </h6>
                        <br />
                        <Row className='w-75 mx-auto d-flex justify-content-center'>
                            <Col xs='5'>
                                <Input
                                    name='code'
                                    id='code'
                                    value={code}
                                    onChange={(e) => setCode(e.target.value)}
                                />
                            </Col>
                            <Col xs='1' className='pt-1'>&#8212;</Col>
                            <Col xs='5'>
                                <Input
                                    name='section'
                                    id='section'
                                    value={section}
                                    onChange={(e) => setSection(e.target.value)}
                                />
                            </Col>
                            <Col xs='12' className='mt-4'>
                                <div className='d-flex justify-content-center py-2'>
                                    <Button color='main-rounded' disabled={!code || !section || loading} onClick={handleSubmit}>
                                        {loading ? <Spinner color='light' size='sm' /> : 'Submit'}
                                    </Button>
                                    
                                </div>
                            </Col>
                            {wrong >= 3 &&
                                <Col xs='12' className='text-center'>
                                    <small className='text-danger'>
                                        Periksa kembali penggunaan huruf besar, huruf kecil, dan angka. Hubungi 
                                        Admin atau petugas jika anda mengalami error terus-menerus, atau muat ulang 
                                        halaman anda.
                                    </small>
                                </Col>
                            }
                        </Row>
                    </CardBody>
                </Card>
            </div>
        )
    }
    else {
        return(
            <>
                {step === 1 ? <ConfirmME handleSubmit={() => setStep(2)} />
                :
                <TutorialME submit={submit} />
                }
            </>
        )
    }
}

const ConfirmME = ({handleSubmit}) => {
    const [agree, setAgree] = useState(false)

    return(
        <div className="page-content">
            <Card>
                <CardBody className="p-5 text-center">
                    <h4>PERHATIAN</h4>
                    <i className='fa fa-4x fa-clock-o my-2' />
                    <h6 className='mt-3 mx-auto' style={{lineHeight:1.8, width:'90%'}}>
                        Di halaman selanjutnya, anda akan diminta untuk menghafalkan beberapa kata selama tiga(3) menit, sebelum beralih ke petunjuk tes selanjutnya. 
                        Anda tidak dapat mengulangi waktu 3 menit menghafal atau langsung menuju halaman petunjuk tes. Oleh karena itu, 
                        pastikan Anda sudah siap untuk mencermati setiap kata yang anda di halaman selanjutnya.
                    </h6>
                    <br />
                    <Row className='w-75 mx-auto d-flex justify-content-center'>
                        <Col xs='12' className='mt-4'>
                            <div className='d-flex justify-content-center py-2'>
                                <CustomInput
                                    type="checkbox"
                                    name="confirm"
                                    id="confirm"
                                    label="Saya sudah siap untuk menuju halaman selanjutnya"
                                    onChange={(e) => setAgree(e.target.checked)}
                                />
                            </div>
                        </Col>
                        <Col xs='12' className='mt-4'>
                            <div className='d-flex justify-content-center py-2'>
                                <Button color='main-rounded' disabled={!agree} onClick={handleSubmit}>
                                    Halaman Berikutnya
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </div>
    )
}

export default CodeME