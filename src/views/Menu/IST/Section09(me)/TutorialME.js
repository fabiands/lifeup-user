import React, {useState, useEffect} from 'react'
import { Button, Card, CardBody, CustomInput, Spinner } from 'reactstrap'
import request from "../../../../utils/request"
import TestME from './TestME'
import OpeningME from './OpeningME'

function TutorialME({submit}){
    const [opening, setOpening] = useState(true)
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [test, setTest] = useState(false)
    const [correct, setCorrect] = useState(false)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
        setLoading(true)
        request.get('/ist/me')
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
            .finally(() => setLoading(false))
    },[])

    const startME = () => {
        setTest(true)
    }

    return(
        <Card className="bg-white">
            <CardBody className="p-4">
            {opening ?
                <OpeningME start={true} submit={() => setOpening(false)} />
            : !test ?
                <>
                    <HintME isModal={false} setCorrect={setCorrect} />
                    <Button color="main-rounded" onClick={startME} disabled={loading || error || !correct}>
                        {loading ? <Spinner color="light" size="sm" /> : error ? 'Error' : 'Mulai Tes'}
                    </Button>
                    {loading ? <small className='text-muted'><br />Mempersiapkan soal...</small> :
                    error ? <small className='text-danger error-reload' onClick={() => window.location.reload()}><br />Error, silahkan muat ulang</small> : null
                    }
                </>
            :
                <TestME start={true} data={data} submit={submit} />
            }
        </CardBody>
        </Card>
    )
}

export const HintME = ({toggle, isModal, setCorrect}) => {

    return(
        <div>
            <div className='text-center'>
                <h3 className="mt-3"><b>Section ME</b></h3>
                <h4>
                    Petunjuk dan Contoh untuk Kelompok Soal 09<br />
                    (Soal-soal No. 157-176)
                </h4>
            </div>
            <div className="w-75 my-5 mx-auto text-left">
                Pada persoalan berikutnya, terdapat sejumlah pertanyaan-pertanyaan mengenai kata-kata
                yang telah teman-teman hafalkan tadi.<br />
                Pilihlah jawaban yang sesuai dengan pertanyaan yang tersedia.
            </div>
            <div className="w-75 mt-4 mb-3 mx-auto text-left">
                <b className='my-2'>Contoh 01</b><br />
                Kata yang mempunyai huruf permulaan -Q- adalah suatu.........<br />
                <div className='d-flex justify-content-between w-75 mb-4'>
                    <CustomInput type="radio" id="example_wu" value="" disabled={true} label="a. bunga" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_wu" value="" disabled={true} label="b. perkakas" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_wu" value="" disabled={true} label="c. burung" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_wu" value="" disabled={true} label="d. kesenian" style={{width:'20%'}} />
                    <CustomInput type="radio" id="example_wu" value="" disabled={true} label="e. binatang" style={{width:'20%'}} />
                </div>
                <div className='mt-4'>
                    Quintet termasuk dalam jenis kesenian, sehingga pilihan <b>d. kesenian</b> harus dipilih.
                </div>
            </div>
            <div className='w-75 mt-2 mb-5 text-center mx-auto d-flex justify-content-between'>
                <CustomInput type="radio" id="example_wu" value="" disabled={true} label="a. bunga" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_wu" value="" disabled={true} label="b. perkakas" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_wu" value="" disabled={true} label="c. burung" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_wu" value="" disabled={true} checked={true} label="d. kesenian" style={{width:'20%'}} />
                <CustomInput type="radio" id="example_wu" value="" disabled={true} label="e. binatang" style={{width:'20%'}} />
            </div>
            <div className="w-75 mt-5 mb-3 mx-auto text-left">
                <b className='my-2'>Contoh berikutnya :</b><br />
                Kata yang mempunyai huruf permulaan -Z- adalah suatu...........<br />
                <div className='d-flex justify-content-between w-75 mt-3 mb-5'>
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_wu_1" name="ex_wu" value="" label="a. bunga" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_wu_2" name="ex_wu" value="" label="b. perkakas" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_wu_3" name="ex_wu" value="" label="c. burung" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(false)} type="radio" id="example_wu_4" name="ex_wu" value="" label="d. kesenian" style={{width:'20%'}} />
                    <CustomInput onClick={() => setCorrect(true)} type="radio" id="example_wu_5" name="ex_wu" value="" label="e. binatang" style={{width:'20%'}} />
                </div>
                Karena Zebra termasuk dalam jenis binatang, maka pilihan <b>e. binatang</b> harus dipilih.
            </div>
            {isModal &&
                <div className='text-center mt-5 mb-3'>
                    <Button color="main-rounded" onClick={toggle}>Mengerti</Button>
                </div>
            }
        </div>
    )

}

export default TutorialME