import React, {useState, useEffect} from 'react'
import { toast } from 'react-toastify'
import { Col, CustomInput, Row, Button, CardBody, Card, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'
import TestTimer from '../../../../components/TestTimer'
import request from '../../../../utils/request'
import { HintME } from './TutorialME'

function TestME({start, data, submit}){
    const local = localStorage.getItem('answers')
    const [timer, setTimer] = useState(false)
    const [answer, setAnswer] = useState(JSON.parse(local)?.me ?? [])
    const [modalSubmit, setModalSubmit] = useState(false)
    const [dataSubmit, setDataSubmit] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [hint, setHint] = useState(false)

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
    },[])


    useEffect(() => {
        if(start){
            setTimer(true)
        }
    },[start])

    const handleChange = (id, value) => {
        const newAnswer = { ...answer, [id]: value }
        setAnswer(newAnswer)
    }

    const handleSubmit = (type) => {
        const section = 'me'
        let arr = []
        for(let i=0; i < 20; i++){
            if(answer[i]){
                arr.push(answer[i])
            }
            else{
                arr.push("")
            }
        }
        const parse = JSON.parse(local)
        let newAnswers = {...parse, [section]: arr}
        localStorage.setItem('answers', JSON.stringify(newAnswers))
        setDataSubmit(JSON.stringify(newAnswers))
        if(type === 'done'){
            setModalSubmit(true)
        }
        else if(type === 'time'){
            handlePost()
        }
    }

    const handlePost = () => {
        setIsLoading(true)
        request.post('/ist', JSON.parse(dataSubmit))
        .then(() => {
            toast.success('Sukses mengerjakan tes IST')
            submit()
        })
        .catch(() => {
            toast.error('Error')
            return;
        })
        .finally(() => setIsLoading(false))
    }

    return(
        <div className='test-page'>
            {timer &&
                <TestTimer
                    test={true}
                    name="me"
                    timeStart={360}
                    onSubmit={() => handleSubmit('time')}
                />
            }
            {data?.length > 0 && data.map((item, idx) => 
            <Card className="rounded shadow-sm my-4 mx-auto" key={idx} style={{width:'85%'}}>
                <CardBody>
                    <Row className="p-3">
                        <Col xs="12" className="text-left mb-1">{item.id+156}. {item.question}</Col>
                        <div className="ml-2 d-flex justify-content-between mt-1 mb-2 w-100 p-0">
                        {item?.answers?.map((ans, index) =>
                            <div key={index} className="py-2 px-3 text-left" style={{width:'20%'}}>
                                <CustomInput
                                    type="radio"
                                    id={`ME_${item.id}_${ans.value}`}
                                    name={`ME_${item.id}_${ans.value}`}
                                    label={ans.value + ". " + ans.label}
                                    value={ans.value}
                                    checked={answer[idx] === ans.value}
                                    onChange={() => handleChange(idx, ans.value)}
                                />
                            </div>
                        )}
                        </div>
                    </Row>
                </CardBody>
            </Card>
            )}
            <Button color="main-rounded" onClick={() => handleSubmit('done')}>Submit</Button>
            <Modal isOpen={modalSubmit} className="rounded" style={{marginTop:'40vh'}}>
                <ModalHeader className="border-bottom-0 bg-transparent">
                    Konfirmasi Submit Tes IST
                </ModalHeader>
                <ModalBody className="py-3 text-center">
                    <h5>Apakah anda yakin ingin mengirim jawaban anda ?</h5>
                    <h6 className='text-muted'>Anda dapat mengecek kembali jawaban di subtes ini karena masih ada waktu</h6>
                </ModalBody>
                <ModalFooter className="border-top-0 text-center">
                    <Button disabled={isLoading} color="netis-danger" className="mr-2" onClick={() => setModalSubmit(false)}>Batal</Button>
                    <Button disabled={isLoading} className="ml-2" color="netis-color" onClick={handlePost}>
                        {isLoading ? 'loading...' : 'Submit'}
                    </Button>
                </ModalFooter>
            </Modal>
            <div className='btn-hint-small'>
                <img src={require('../../../../assets/icon-hint.png')} alt="hint" className='scale-div' width={50} height={50}
                    onClick={() => setHint(true)}
                />
            </div>
            <Modal className='modal-hint' isOpen={hint} size="lg" toggle={() => setHint(false)} style={{borderRadius:'15px'}}>
                <ModalBody className='px-5'>
                    <HintME isModal={true} handleChange={(ans) => console.log(ans)} toggle={() => setHint(false)} />
                </ModalBody>
            </Modal>
        </div>
    )
}

export default TestME