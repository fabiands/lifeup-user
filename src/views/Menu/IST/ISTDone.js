import React from 'react'
import { useHistory } from 'react-router-dom'
import { Button, Card, CardBody } from 'reactstrap'

function ISTDone({clear}){
    const history = useHistory()

    const onDoneIST = () => {
        localStorage.removeItem('answers')
        history.push('/rmib')
    }

    return(
        <Card className="bg-white">
            <CardBody className="p-4">
                <h3 className="my-5">
                    {clear ? 'Anda telah mengerjakan Test IST'
                    :
                    'Terima kasih telah mengerjakan Tes IST'
                    }
                </h3>
                <div className="mt-2 w-75 mx-auto d-flex justify-content-center align-items-center">
                    <img src={require('../../../assets/done.svg')} alt="dashboard" width={200} />
                </div>
                <p className='my-5'>
                    Silahkan melanjutkan test tahap selanjutnya
                </p>
                <Button color="main-rounded" className="mb-4" onClick={onDoneIST}>Mulai Tes Kedua</Button>
            </CardBody>
        </Card>
    )
}

export default ISTDone