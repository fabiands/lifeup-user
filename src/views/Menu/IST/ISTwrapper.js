import React, { useEffect, useState } from 'react'
import { Prompt } from "react-router-dom";
import ISTDone from './ISTDone'
import CodeSE from "./Section01(se)/CodeSE"
import CodeWA from "./Section02(wa)/CodeWA"
import CodeAN from './Section03(an)/CodeAN'
import CodeGE from './Section04(ge)/CodeGE'
import CodeRA from './Section05(ra)/CodeRA'
import CodeZR from './Section06(zr)/CodeZR'
import CodeFA from './Section07(fa)/CodeFA'
import CodeWU from './Section08(wu)/CodeWU'
import CodeME from './Section09(me)/CodeME'
import {useAuthUser} from '../../../store'

function ISTWrapper(){
    const user = useAuthUser();
    const [activeSection, setActiveSection] = useState(null)
    const [clear, setClear] = useState(false)

    useEffect(() => {
        if(user.ist){
            setActiveSection('done')
            setClear(true)
        }
        else{
            setActiveSection('se')
        }
    }, [user])

    const submit = (section) => {
        setActiveSection(section)
    }

    return(
        <div className={(activeSection === 'fa' || activeSection === 'wu') ? 'page-content-large' : 'page-content'}>
            {activeSection !== 'done' &&
                <Prompt
                    when={(window.onbeforeunload = () => true)}
                    message={"Apakah anda yakin ingin meninggalkan halaman ini?"}
                />
            }
            {activeSection === 'se' && <CodeSE submit={() => submit('wa')} />}
            {activeSection === 'wa' && <CodeWA submit={() => submit('an')} />}
            {activeSection === 'an' && <CodeAN submit={() => submit('ge')} />}
            {activeSection === 'ge' && <CodeGE submit={() => submit('ra')} />}
            {activeSection === 'ra' && <CodeRA submit={() => submit('zr')} />}
            {activeSection === 'zr' && <CodeZR submit={() => submit('fa')} />}
            {activeSection === 'fa' && <CodeFA submit={() => submit('wu')} />}
            {activeSection === 'wu' && <CodeWU submit={() => submit('me')} />}
            {activeSection === 'me' && <CodeME submit={() => submit('done')} />}
            {activeSection === 'done' && <ISTDone clear={clear} />}
        </div>
    )
}

export default ISTWrapper