import React, {useState, useEffect} from 'react'
import { Button, Card, CardBody, CustomInput, Spinner } from 'reactstrap'
import request from "../../../../utils/request"
import TestZR from './TestZR'

function TutorialZR({submit}){
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [test, setTest] = useState(false)
    const [contain, setContain] = useState(false)
    const [correct, setCorrect] = useState([])

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'})
        setLoading(true)
        request.get('/ist/zr')
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
            .finally(() => setLoading(false))
    },[])

    const handleChange = (value, checked) => {
        const a = [0,1]
        let num = correct ?? []

        if(checked){
            num.push(value)
        }
        else if(!checked){
            let set = new Set(num)
            set.delete(value)
            num = Array.from(set)
        }
        
        if(num.length === 2){
            if(a.every(i => num.includes(i))){
                setContain(true)
            }
            else {
                setContain(false)
            }
        }
        else {
            setContain(false)
        }

        setCorrect(num)
    }

    const startZR = () => {
        setTest(true)
    }


    return(
        <Card className="bg-white">
            <CardBody className="p-4">
            {!test ?
                <>
                    <HintZR handleChange={handleChange} isModal={false} />
                    <Button color="main-rounded" onClick={startZR} disabled={loading || error || !contain}>
                        {loading ? <Spinner color="light" size="sm" /> : error ? 'Error' : 'Mulai Tes'}
                    </Button>
                    {loading ? <small className='text-muted'><br />Mempersiapkan soal...</small> :
                    error ? <small className='text-danger error-reload' onClick={() => window.location.reload()}><br />Error, silahkan muat ulang</small> : null
                    }
                </>
            :
            <TestZR start={true} data={data} submit={submit} />
            }
        </CardBody>
        </Card>
    )
}

export const HintZR = ({handleChange, isModal, toggle}) => {
    const width = isModal ? 'w-100' : 'w-75'

    return(
        <div className={isModal ? 'px-3' : ''}>
            <div className='text-center'>
                <h3 className="mt-3"><b>Section ZR</b></h3>
                <h4>
                    Petunjuk dan Contoh untuk Kelompok Soal 06<br />
                    (Soal-soal No. 97-116)
                </h4>
            </div>
            <div className={`${width} my-5 mx-auto text-left`}>
                Pada persoalan berikut akan diberikan deret angka.<br />
                Setiap deret tersusun menurut suatu aturan tertentu dan dapat dilanjutkan menurut 
                aturan tersebut.<br />
                Carilah untuk setiap deret, angka berikutnya dan pilihlah jawaban dengan angka yang sesuai
            </div>
            <div className={`${width} mt-3 mb-3 mx-auto text-left`}>
                <b className='my-2'>Contoh 06</b><br />
                <div className='mt-4 d-flex justify-content-between'>
                    <div style={{width:'10%'}} className='text-center px-auto'>2</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>4</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>6</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>8</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>10</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>12</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>14</div>
                    <div style={{width:'10%'}} className='text-center px-auto'><b>?</b></div>
                </div>
                <div className="mt-3">
                    Pada deret diatas, angka berikutnya selalu didapat jika angka depannya ditambah dengan 2.<br />
                    Maka jawabannya ialah 16.<br />
                    Oleh karena itu, angka <b>1</b> dan <b>6</b> harus dipilih
                </div>
                <div className="d-flex justify-content-between my-2 w-100">
                    <CustomInput type="checkbox" name="example_zr1" id="ex_zr1" value="" disabled={true} checked={true} label={1} />
                    <CustomInput type="checkbox" name="example_zr1" id="ex_zr1" value="" disabled={true} label={2} />
                    <CustomInput type="checkbox" name="example_zr1" id="ex_zr1" value="" disabled={true} label={3} />
                    <CustomInput type="checkbox" name="example_zr1" id="ex_zr1" value="" disabled={true} label={4} />
                    <CustomInput type="checkbox" name="example_zr1" id="ex_zr1" value="" disabled={true} label={5} />
                    <CustomInput type="checkbox" name="example_zr1" id="ex_zr1" value="" disabled={true} checked={true} label={6} />
                    <CustomInput type="checkbox" name="example_zr1" id="ex_zr1" value="" disabled={true} label={7} />
                    <CustomInput type="checkbox" name="example_zr1" id="ex_zr1" value="" disabled={true} label={8} />
                    <CustomInput type="checkbox" name="example_zr1" id="ex_zr1" value="" disabled={true} label={9} />
                    <CustomInput type="checkbox" name="example_zr1" id="ex_zr1" value="" disabled={true} label={0} />
                </div>
            </div>
            <div className={`${width} my-5 mx-auto text-left`}>
                <b className='my-2'>Contoh lain :</b>
                <div className='mt-4 d-flex justify-content-between'>
                    <div style={{width:'10%'}} className='text-center px-auto'>9</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>7</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>10</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>8</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>11</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>9</div>
                    <div style={{width:'10%'}} className='text-center px-auto'>12</div>
                    <div style={{width:'10%'}} className='text-center px-auto'><b>?</b></div>
                </div>
                <div className='mt-2'>
                    Pada deret diatas, pola selalu berganti dengan pengurangan 2 terlebih dahulu, kemudian 
                    penambahan 3 angka.<br />
                    Maka, jawabannya ialah 10 sehingga angka <b>1</b> dan <b>0</b> harus dipilih
                </div>
                <div className="d-flex justify-content-between mt-2 mb-5 w-100">
                    <CustomInput type="checkbox" onChange={(e) => handleChange(1, e.target.checked)} name="example_zr2" id="example_zr1" value={1} label={1} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(2, e.target.checked)} name="example_zr2" id="example_zr2" value={2} label={2} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(3, e.target.checked)} name="example_zr2" id="example_zr3" value={3} label={3} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(4, e.target.checked)} name="example_zr2" id="example_zr4" value={4} label={4} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(5, e.target.checked)} name="example_zr2" id="example_zr5" value={5} label={5} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(6, e.target.checked)} name="example_zr2" id="example_zr6" value={6} label={6} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(7, e.target.checked)} name="example_zr2" id="example_zr7" value={7} label={7} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(8, e.target.checked)} name="example_zr2" id="example_zr8" value={8} label={8} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(9, e.target.checked)} name="example_zr2" id="example_zr9" value={9} label={9} />
                    <CustomInput type="checkbox" onChange={(e) => handleChange(0, e.target.checked)} name="example_zr2" id="example_zr0" value={0} label={0} />
                </div>
                <div className='mt-2'>
                    Pada beberapa soal, pencarian pola juga dapat dilakukan dengan perkalian dan pembagian.
                </div>
            </div>
            {isModal &&
                <div className='text-center mt-5 mb-3'>
                    <Button color="main-rounded" onClick={toggle}>Mengerti</Button>
                </div>
            }
        </div>
    )
}

export default TutorialZR