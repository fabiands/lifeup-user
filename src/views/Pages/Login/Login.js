import React, { Component } from 'react';
import { DatePickerInput } from 'rc-datepicker';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, FormGroup, Input, Label, Row, Spinner,UncontrolledTooltip  } from 'reactstrap';
import { login } from "../../../actions/auth";
import moment from 'moment'
import {translate} from 'react-switch-lang'
import { connect } from "react-redux";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      birth_date: '',
      code: '',
      update: '0',
      nullForm: false
    };
  }

  async loginProcess() {
    let birth_date = moment(this.state.birth_date).format('YYYY-MM-DD');
    let code = this.state.code;
    await this.props.login({ birth_date, code });
    this.setState({
      update: '1',
    });
  }
  handleLogin = (e) => {
    e.preventDefault();
    this.loginProcess();
  }
  handleCode = (e) => {
    const value = e.target.value ?? ""
    if(this.state.nullForm){
      if(value){
        this.setState({nullForm:false})
      }
    }
    if(value){
      this.setState({code: value.toUpperCase()})
    }
    else {
      this.setState({code: ''})
    }
  }
  handleBlur = () => {
    if(!this.state.code){
      this.setState({nullForm:true})
    }
  }

  handleDate = (e) => {
    this.setState({birth_date: e})
  }

  render(){
    moment.locale('id')
    return(
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="12">
              <CardGroup className="card-login-group shadow-lg">
                <Card className="card-login-info d-sm-down-none">
                  <CardBody className="text-center p-5">
                      <h2>LifeUp Assessment</h2>
                  </CardBody>
                </Card>
                <Card className="card-login-form">
                  <CardBody className="flex-column p-5">
                    <div className="mt-2 mb-4 text-center">
                      <h5>Selamat datang di Web LifeUp Assessment</h5>
                      <p className='text-muted'>Silahkan login dengan memasukkan tanggal lahir dan kode yang telah diberikan</p>
                    </div>
                    <Form className="my-auto">
                      <FormGroup className="mb-4 mt-2">
                        <Label htmlFor="birth_date" className="input-label d-flex justify-content-between pr-1">
                          <>
                            Tanggal Lahir
                            <span className="text-danger">*</span>
                          </>
                          <i id="birthTooltip" className="fa fa-info-circle text-info mt-2" />
                          <UncontrolledTooltip target="birthTooltip" placement="left">
                            Isikan tanggal lahir anda dengan sesuai.
                          </UncontrolledTooltip>
                        </Label>
                          <DatePickerInput
                            name="birth_date"
                            id="birth_date"
                            onChange={this.handleDate}
                            value={this.state.birth_date}
                            className='my-custom-datepicker-component'
                            showOnInputClick={true}
                            closeOnClickOutside={true}
                            displayFormat="DD MMMM YYYY"
                          />
                      </FormGroup>
                      <FormGroup className="mt-2 mb-4">
                        <Label htmlFor="code" className="input-label d-flex justify-content-between pr-1">
                          <>
                            Kode
                            <span className="text-danger">*</span>
                          </>
                          <i id="codeTooltip" className="fa fa-info-circle text-info mt-2" />
                          <UncontrolledTooltip target="codeTooltip" placement="left">
                            Isikan kode milik anda sendiri sesuai dengan kode yang telah diberikan oleh admin.
                          </UncontrolledTooltip>
                        </Label>
                        <Input type="text" id="code" name="code" placeholder="Masukkan kode anda" onChange={this.handleCode} onBlur={this.handleBlur} value={this.state.code} />
                        <small className={this.state.nullForm ? 'text-danger' : 'text-white'}>Isikan kode anda</small>
                      </FormGroup>
                      <FormGroup className="w-50 text-center my-3 mx-auto">
                        <Button disabled={!this.state.birth_date || !this.state.code || this.props.isLoading} onClick={this.handleLogin} className="login-submit mt-4 mb-2 py-2">
                          {this.props.isLoading ? <Spinner size="sm" color="dark" /> : 'Login'}
                        </Button>
                      </FormGroup>
                      <div>
                        <span className="text-danger">*</span>
                        <small className="text-muted"><i>
                        Jika anda belum memiliki kode atau tidak berhasil melakukan login, silahkan 
                        hubungi admin
                        </i></small>
                      </div>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}


const mapStateToProps = (state) => ({
  error: state.error,
  isLoading: state.isLoading
})

export default connect(mapStateToProps, { login })(translate(Login));
