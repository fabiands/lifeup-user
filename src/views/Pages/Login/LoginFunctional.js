import React, { useState } from 'react';
import { DatePickerInput } from 'rc-datepicker';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, FormGroup, Input, Label, Row, Spinner } from 'reactstrap';
import { login } from "../../../actions/auth";
import moment from 'moment'
import request from '../../../utils/request';

export default function Login() {
    moment.locale('id')
    const [birthday, setBirthday] = useState(new Date())
    const [code, setCode] = useState("")
    const [loading, setLoading] = useState(false)
    // console.log(process?.env?.REACT_APP_DOMAIN)
    const handleLogin = (e) => {
        e.preventDefault();
        setLoading(true)
        // console.log('login')
        // login({birthday, code})
        request.post('/login', { birth_date: birthday, code })
            .then((res) => {
                console.log(res)
            })
            .catch((err) => {
                console.log(err)
            })
            .finally(() => {
                setLoading(false)
            })
    }
    const handleCode = (e) => {
        const value = e.target.value ?? ""
        if (value) {
            setCode(value.toUpperCase())
        }
        else {
            setCode("")
        }
    }
    const handleDate = (e) => {
        const value = moment(e).format('YYYY MMMM DD')
        setBirthday(value)
    }

    return (
        <div className="app flex-row align-items-center">
            <Container>
                <Row className="justify-content-center">
                    <Col md="12">
                        <CardGroup className="card-login-group shadow-lg">
                            <Card className="card-login-info d-sm-down-none">
                                <CardBody className="text-center">
                                    <h2>Assesment Web</h2>
                                </CardBody>
                            </Card>
                            <Card className="card-login-form">
                                <CardBody className="flex-column">
                                    <div className="mt-2 mb-4 text-center">
                                        <h5>Selamat datang di Web Tes Psikologi</h5>
                                        <p>Silahkan login</p>
                                    </div>
                                    <Form onSubmit={handleLogin} className="my-auto">
                                        <FormGroup className="mb-4 mt-2">
                                            <Label htmlFor="birthday" className="input-label">Tanggal Lahir</Label>
                                            <DatePickerInput
                                                name="birthday"
                                                id="birthday"
                                                onChange={handleDate}
                                                value={birthday}
                                                className='my-custom-datepicker-component'
                                                showOnInputClick={true}
                                                closeOnClickOutside={true}
                                                displayFormat="DD MMMM YYYY"
                                            />
                                        </FormGroup>
                                        <FormGroup className="mt-2 mb-4">
                                            <Label htmlFor="code" className="input-label">Kode</Label>
                                            <Input type="text" id="code" name="code" placeholder="Masukkan kode anda" onChange={handleCode} value={code} />
                                        </FormGroup>
                                        <FormGroup className="w-50 text-center my-3 mx-auto">
                                            <Button disabled={!birthday || !code || loading} type="submit" className="login-submit mt-4 mb-2 py-2">
                                                {loading ? <Spinner size="sm" color="dark" /> : 'Login'}
                                            </Button>
                                        </FormGroup>
                                    </Form>
                                </CardBody>
                            </Card>
                        </CardGroup>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
