import React, {useState, useEffect} from 'react'
import { CountdownCircleTimer } from "react-countdown-circle-timer";

export default function TestTimer({test=false, timeStart=360, onSubmit, name=''}){
    const [isTime, setIsTime] = useState(false)

    useEffect(() => {
        if(test){
            setIsTime(true)
        }
    },[test])

    const renderTime = ({ remainingTime }) => {
        if (remainingTime === 0) {
            return <div className="timer">Time's up</div>;
        }

        return (
            <div className="timer">
                <div className="text">{name}</div>
                <div className="value">{remainingTime}</div>
                <div className="text">seconds</div>
            </div>
        );
    };

    const handleComplete = () => {
        setIsTime(false)
        onSubmit()
    }

    return(
        <>
            {isTime &&
                <div className="countdown-fixed d-none">
                    <CountdownCircleTimer
                        isPlaying
                        duration={timeStart}
                        colors={["#004777", "#F7B801", "#A30000", "#A30000"]}
                        colorsTime={[10, 8, 5, 3]}
                        onComplete={handleComplete}
                        size={50}
                    >
                        {renderTime}
                    </CountdownCircleTimer>
                </div>
            }
        </>
    )
}