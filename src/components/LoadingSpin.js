import React from "react"
import Loader from "react-loader-spinner";

function LoadingSpin({ type = 'Oval', color = '#385b4f', secondaryColor = "#00BFFF" }) {
    return (
        <div className="loading">
            <div className="spinner-loading">
                <Loader color={color} secondaryColor={secondaryColor} type={type} height={150} width={150} radius={1} /><br />
                <h4 className="mt-3" style={{color:'#385b4f'}}>Mohon menunggu...</h4>
            </div>
        </div>
    )
}

export default LoadingSpin;