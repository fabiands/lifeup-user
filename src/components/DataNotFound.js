import React from 'react';
import Illustration from '../assets/nodata.svg';

const DataNotFound = () => {
    return (
        <div className="text-center my-3">
            <img src={Illustration} alt="not found" width={250} />
            <br />
            <h6 className='text-muted'>Data Tidak Ditemukan</h6>
        </div>
    );
}

export default DataNotFound;
