import React from 'react';
import { Button, Modal, ModalBody } from 'reactstrap';
import DataNotFound from './DataNotFound'

const ModalError = ({isOpen=true}) => {
    return(
        <div className='loading'>
        <DataNotFound />
        <Modal isOpen={isOpen} className="modal-md" backdropClassName="back-home" style={{marginTop: '10vh'}}>
            <ModalBody>
                <div className="row justify-content-center mt-2">
                    <div className="col-12">
                        <div className="text-center" style={{ borderRadius: "5px" }}>
                            <i className="fa fa-2x fa-exclamation-triangle mb-2 text-danger" /><br />
                            <h5 className="mt-3">
                                Terjadi kesalahan pemuatan, silahkan muat ulang halaman anda
                            </h5>
                            <h6 className='mb-3 text-muted'>
                                Silahkan periksa koneksi internet atau perangkat anda jika terjadi kesalahan 
                                pemuatan berkali-kali
                            </h6>
                            <Button color="netis-outline" className="px-3" onClick={() => window.location.reload()}>
                                Muat ulang
                            </Button>
                        </div>
                    </div>
                </div>
            </ModalBody>
        </Modal>
        </div>
    )
}

export default ModalError