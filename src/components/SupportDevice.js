import React, {useState} from 'react'
import {Modal, ModalBody, Button, ModalHeader} from 'reactstrap'

function SupportDevice({isOpen}){
    const [open, setOpen] = useState(isOpen ?? true)

    return(
        <Modal isOpen={open} className='modal-rounded'>
            <ModalHeader className='d-flex justify-content-center border-bottom-0'>SUPPORTED DEVICE</ModalHeader>
            <ModalBody className='py-3 px-4'>
                <div className='text-center mt-2 mb-4'>
                    <img src={require('../assets/support.svg')} alt="support" width={150} />
                </div>
                Browser yang direkomendasikan adalah&nbsp;
                <b>Google Chrome</b>&nbsp;
                (<i className='fa fa-chrome' />)&nbsp;
                dan pastikan Pengaturan Javascript di browser anda aktif.
                <hr />
                Jika anda tidak menggunakan atau tidak memiliki Google Chrome, pastikan 
                Javascript anda aktif, dan browser anda sudah diperbarui ke versi paling baru.
                <br /><br />
                <i><b>*NB :</b> Tidak direkomendasikan menggunakan Internet Explorer</i>
                <div className='my-3 text-center'>
                    <Button color="main-rounded" className="mt-3" onClick={() => setOpen(false)}>
                        Mengerti
                    </Button>
                </div>
            </ModalBody>
        </Modal>
    )
}

export default SupportDevice