import React from 'react';
import {Spinner} from 'reactstrap'

const LoadingGrow = () => {
    return (
        <div className="loading">
            <div className='spinner-loading'>
                <Spinner color="dark" type="grow" /><br />
                <h4 className='mt-3' style={{color:'#385b4f'}}>Mohon menunggu...</h4>
            </div>
        </div>
    );
}

export default LoadingGrow;
