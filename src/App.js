import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";
import "./App.scss";
import { Provider } from "react-redux";
import AuthRoute from "./components/AuthRoute.js";
import OfflineIndicator from "./components/OfflineIndicator.js";
import LoginPage from "./views/Pages/Login/Login.js";
import store from "./store";
import Layout from "./containers/Layout";
import moment from "moment";
import momentLocales from './utils/language/moment-locales';
import { toast } from 'react-toastify';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(fab, fas)
toast.configure();

moment.updateLocale('id', momentLocales.id);

export default function App() {
  return (
    <Provider store={store}>
      <OfflineIndicator />
      <Router>
        <Switch>
          <AuthRoute path="/" exact>
            <Redirect to="/login" />
          </AuthRoute>
          <AuthRoute path="/login" type="guest" exact>
            <LoginPage />
          </AuthRoute>
          <AuthRoute type="private" exact component={Layout} />
        </Switch>
      </Router>
    </Provider>
  );
}
