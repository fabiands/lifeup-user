import React, { Component } from "react";
import { withRouter } from "react-router";
import {
  Nav,
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { AppNavbarBrand } from "@coreui/react";
import logo from "../assets/icon.png";
import icon from "../assets/icon.png";
import { connect } from "react-redux";
import { logout } from "../actions/auth";

// setLanguageCookie()
class DefaultHeader extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: props.user,
      session: props.token,
      confirmLogout: false
    };

  }
  
  handleLogout = () => {
    localStorage.removeItem('answers')
    localStorage.removeItem('rmib')
    this.setState({confirmLogout: !this.state.confirmLogout})
  }

  render() {
    return (
      <React.Fragment>
        <AppNavbarBrand
          style={{
            position: "initial",
            top: "unset",
            left: "unset",
            marginLeft: 0,
            paddingLeft:'35px',
            justifyContent:'start'
          }}
          full={{ src: logo, height: 30, alt: "Logo" }}
          minimized={{ src: icon, height: 30, alt: "Icon" }}
        />
        <h5><b>LifeUp Assessment</b></h5>
        <Nav navbar>
          <Row>
            <Col xs="8" className="text-right" style={{fontSize:'13px'}}>
              <small className="text-muted mb-0">Welcome</small><br />
              <strong>{this.state.user?.fullName ?? this.props?.user?.data?.fullName}</strong>
            </Col>
            <Col xs="4" className="d-flex align-items-center justify-content-center">
              <Button className="bg-transparent border-0" onClick={this.handleLogout}>
                <i className="fa fa-sign-out logout-icon" />
              </Button>
            </Col>
          </Row>
        </Nav>
        <Modal isOpen={this.state.confirmLogout} className="modal-rounded" style={{marginTop:'30vh'}} toggle={this.handleLogout}>
          <ModalBody className="text-center mt-5 mb-2">
            <h5>Apakah anda yakin ingin keluar?</h5>
            <h6>Jika anda sedang mengerjakan tes, jawaban anda yang sedang anda kerjakan dan belum di-submit tidak akan tersimpan 
              dan anda harus mengerjakan ulang tes tersebut.
            </h6>
          </ModalBody>
          <ModalFooter className="border-top-0">
            <Button onClick={this.handleLogout} color="netis-danger"><i className="fa fa-times mr-2" />Batal</Button>
            <Button onClick={this.props.logout} color="netis-color"><i className="fa fa-sign-out mr-2" />Keluar</Button>
          </ModalFooter>
        </Modal>
      </React.Fragment >
    );
  }
}

const mapStateToProps = ({ user, token }) => ({
  user,
  token,
});
export default connect(mapStateToProps, { logout })(
  withRouter(DefaultHeader)
);
