import { lazyComponent as lazy } from '../components/lazyComponent';

const buildMenu = () => [
    {
        url: "/dashboard",
        exact:true,
        component: lazy(() =>
            import("../views/Menu/Dashboard")
        ),
    },
    {
        url: "/ist",
        exact:true,
        component: lazy(() =>
            import("../views/Menu/IST/ISTwrapper")
        )
    },
    {
        url: "/rmib",
        exact:true,
        component: lazy(() =>
            import("../views/Menu/RMIB/RMIBWrapper")
        )
    },
];

var cachedRoutes;
const menu = () => {
    if (! cachedRoutes) {
        cachedRoutes = buildMenu();
    }

    return cachedRoutes;
}

export default menu;
