import React, { Component, Suspense } from 'react';
import * as router from 'react-router-dom';
import { AppHeader } from '@coreui/react';
import { translate } from 'react-switch-lang';
import DefaultHeader from './DefaultHeader';
import { Container } from 'reactstrap';
import menu from './menu'
import LoadingGrow from '../components/LoadingGrow';
import AuthRoute from '../components/AuthRoute';
import { connect } from 'react-redux';
import { setLanguage } from 'react-switch-lang';
class DefaultLayout extends Component {
    componentDidMount() {
        setLanguage(localStorage.getItem('language'))
    }

    generateRoutes(menu) {
        return menu()
            .map((props, idx) =>
                !!props.redirect ?
                    <router.Redirect from={props.url} to={props.redirect} exact={!!props.exact} />
                    :
                    <AuthRoute key={idx} path={props.url} exact={!!props.exact} component={props.component} {...props} />
            );
    }

    render() {
        const { Switch, Redirect } = router;

        return (
            <div className="app">
                <AppHeader fixed><DefaultHeader /></AppHeader>
                <div className="app-body">
                    <main className="main">
                        <Container fluid>
                            <Suspense fallback={<LoadingGrow />}>
                                <Switch>
                                    <Redirect exact from="/home" to="/dashboard" />
                                    {this.generateRoutes(menu)}
                                </Switch>
                            </Suspense>
                        </Container>
                    </main>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (reduxState) => ({ user: reduxState.user })

export default connect(mapStateToProps)(translate(DefaultLayout));
