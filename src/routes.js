import React from 'react';

const Dashboard = React.lazy(() => import('./views/Menu/Dashboard'));

const routes = [
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
];

export default routes;
