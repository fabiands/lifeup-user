export function arrayShuffle(array) {
    var currentIndex = array.length,
        temporaryValue,
        randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

export function arrayGroupBy(array, groupBy) {
    const result = {};
    for (let item of array) {
        const key = typeof groupBy === 'function' ? groupBy(item) : item[groupBy];
        if (! result[key]) {
            result[key] = [item];
        } else {
            result[key].push(item);
        }
    }

    return result;
}

export function objectGet(obj, needle) {
    const keys = needle.split('.');
    let findObj = obj;
    for (let key of keys) {
        if (findObj[key] === undefined || findObj[key] === null) {
            return null;
        } else {
            findObj = findObj[key];
        }
    }

    return findObj;
}
