import request from './request'
import { toast } from 'react-toastify'
import { t } from 'react-switch-lang';
toast.configure()
export const DeleteData = async (url, id) => {

    try {
        // eslint-disable-next-line
        const response = await request.delete(url + '/' + id);
        toast.success(t('Berhasil dihapus'));
        return true;
    } catch (err) {
        if (err.response && err.response.data) {
            toast.error(err.response.data.message || 'Error')
        }
        return false;
    }
}